
var errors={
    "password":"Passwörter stimmen nicht überein"
};
$(document).ready(function(){




   $('[data-toggle="tooltip"]').tooltip(); 
    var amountediting = false;
   $('.amount').change(function(){
    if(amountediting) return;
    amountediting = true;
    var content = $(this).val();
     content = content.split('.').join(',');
    
     var comma = content.lastIndexOf(',');
      console.log(comma);
     if(comma != -1 && comma != content.length -1){
      content += "00";
      content = content.slice(0,comma +3);
        
          
     }else{
      
       content = content.split(',').join('');
      
        content += ",00";
        
     }
    
   // content = content.slice(0, comma) + "," + content.slice(comma, comma +2);
  var comma = content.lastIndexOf(',');
    var contentlength = content.length;
  content = content.split(',').join('');
   var numberOfCommas = contentlength - content.length;
   comma -= (numberOfCommas -1);
  content = content.slice(0, comma ) + "," + content.slice(comma, content.length);
   $(this).val(content);
   amountediting = false;
   }); 
   
  
    
    $('.email').change(function(){
       if(!isValidEmailAddress($(this).val())){
           addError($(this), "invalid_email");
        
       }else{
          removeError($(this), "invalid_email");
       } 
    });
  $('.error_tracking').change(function(){
   markErrors();
   
    });

});
 function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
    }
    
    
 function addError(element, error){
        var errors = element.attr('data-errors');
           
        if(errors == undefined || error.indexOf(error) == -1){
            if(errors == undefined){
               
                element.attr("data-errors", error);
            }else{
                element.attr("data-errors", errors+" "+error);
            }
        }
        markErrors();
   }
  
    function removeError(element, error){
        var errors = element.attr('data-errors');
        if(errors == undefined) return;
        errors = errors.split(error).join("");
        element.attr("data-errors", errors);
        if (!errors.replace(/\s/g, '').length) {
         element.removeAttr("data-errors");   
        }
        markErrors();
   }
   function hasError(element){
  
    return element.attr("data-errors") != undefined;
    
   }
  
   
   function markErrors(){
    var errorFound = false;
        $('.error_tracking').each(function(){
           
       
        if(hasError($(this))){
             errorFound = true;
            $(this).addClass("has-error");
           
            $(this).attr("data-original-title", errors[$(this).attr("data-errors").split(' ')[0]]);
            $(this).attr("data-placement", "left");
            $(this).tooltip();
        }else{
           $(this).attr("data-original-title", "");
            
            $(this).removeClass("has-error");
            
        }
       });
       
       if(errorFound){
        $('#submit').attr("disabled", "disabled");
       }else{
        $('#submit').removeAttr("disabled");

       }
   }
    