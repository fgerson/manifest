<?php
namespace AppBundle\Service;
use Doctrine\ORM\EntityManagerInterface;
class DebitService{
    private $em;
    private $debitRepository;
    public function __construct(
        \AppBundle\Repository\DebitRepository $debitRepository,
        EntityManagerInterface $em
    ){
        $this->debitRepository = $debitRepository;
        $this->em = $em;
    }

    public function getBalance(){
        $cash = 0;
        $debits = $this->debitRepository->findAll();
        foreach($debits as $debit){
            $cash += $debit->getSum();
        }
        return $cash;
    }

}