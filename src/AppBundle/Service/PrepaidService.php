<?php
namespace AppBundle\Service;
use AppBundle\Entity\Jumptype;
use AppBundle\Entity\Prepaid;
use AppBundle\Entity\PrepaidType;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
class PrepaidService{

    private $prepaidRepository;
    private $jumpGroupRepository;
    private $em;
    public function __construct(\AppBundle\Repository\PrepaidRepository $prepaidRepository,
                                \AppBundle\Repository\JumpGroupRepository $jumpGroupRepository,

                                EntityManagerInterface $em){
        $this->prepaidRepository = $prepaidRepository;
        $this->jumpGroupRepository = $jumpGroupRepository;
        $this->em = $em;
    }
   public function getRemainingPrepaids(User $user, Jumptype $jumptype){
        $capacity = 0;
        $allPrepaids = $this->prepaidRepository->findByUserAndJumpType($user, $jumptype);
        foreach($allPrepaids as $prepaid){
            $capacity += $prepaid->getPrepaidType()->getQuantity();
       }
       $usedPrepaids = $this->jumpGroupRepository->findByUserAndJumpType($user, $jumptype);

       foreach($usedPrepaids as $usedPrepaid){
           $capacity -= 1;
       }
       return $capacity;
   }
    public function getCapacityForPrepaid(Prepaid $prepaid){
        $capacity = $prepaid->getPrepaidType()->getQuantity();
        $used = count($this->jumpGroupRepository->findByPrepaid($prepaid));
        return $capacity -$used;
    }
    public function getAvailablePrepaid(User $user, Jumptype $jumptype){



        $allPrepaids = $this->prepaidRepository->findByUserAndJumpType($user, $jumptype);
        foreach($allPrepaids as $prepaid){
           if($this->getCapacityForPrepaid($prepaid) > 0) return $prepaid;
        }

    }
}