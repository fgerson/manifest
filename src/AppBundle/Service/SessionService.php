<?php
namespace AppBundle\Service;
use AppBundle\Entity\Session;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Util\HashUtil;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SessionService{
    
    private $session;
    private $userRepository;
    private $sessionRepository;

    private $em;
    public function __construct(
   // \Symfony\Component\HttpFoundation\Session\SessionInterface $session,
    \AppBundle\Repository\LoginUserRepository $loginUserRepository,
    \AppBundle\Repository\SessionRepository $sessionRepository,

     EntityManagerInterface $em
    
    )
    {
     //   $this->session = $session;
        $this->userRepository = $loginUserRepository;
        $this->sessionRepository = $sessionRepository;

        $this->em = $em;
    }

    /**
     * @param SessionInterface $session
     * @required
     */
    public function injectSession(SessionInterface $session){
        $this->session = $session;
    }

    public function logout(){
       $this->sessionRepository->deleteSession($this->getCurrentUser());
        $this->session->clear();
    }
    public function login($username, $password){
        $user = $this->userRepository->findOneByUsername($username);
        if($user == null || !$user->getIsActive() ) return false;
        if($user->getPassword() == HashUtil::SHA256($password)){
            $session = new Session();

            $session->setUser($user);
            $session->setLastUsed(new \DateTime());
            $session->setSessionKey(HashUtil::getUUID());
            $this->em->persist($session);
            $this->em->flush();
            $this->session->clear();
            $this->session->set("sessionkey", $session->getSessionKey());
 			$userA = array("id" => $user->getId(), "username" => $user->getUsername(), "first_name" =>$user->getFirstName(), "last_name" =>$user->getLastName());
            $this->session->set("user", $userA);


            return true;
        }
        return false;
    }

    public function getUserBySessionKey($sessionkey){
        $session = $this->sessionRepository->findOneBySessionKey($sessionkey);
        if($session == null || $session->getDeleted()) return null;
        $this->setTrackingData($sessionkey);
        return $session->getUser();
    }

    public function getCurrentUser(){
        if($this->session->get("sessionkey") == null) return null;
      $session = $this->sessionRepository->findOneBySessionKey($this->session->get("sessionkey"));
        if($session == null || $session->getDeleted()){
             $this->session->clear();
             return null;
        }
        $this->setTrackingData($session->getSessionKey());
        return $session->getUser();
    }



    private function setTrackingData($sessionKey){
        $session = $this->sessionRepository->findOneBySessionKey($sessionKey);
        $session->setLastUsed(new \DateTime());
        $session->setIp($_SERVER['REMOTE_ADDR']);
        $this->em->merge($session);
        $this->em->flush();
    }
   
}