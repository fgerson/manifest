<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Iban;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Validation;

class UserService
{

    private $userRepository;
    private $em;

    public function __construct(\AppBundle\Repository\UserRepository $userRepository, EntityManagerInterface $em)
    {
        $this->userRepository = $userRepository;
        $this->em = $em;
    }

    public function updateUser(User $user, $user_id, $qualifications = array())
    {
        $qualificationRepository = $this->em->getRepository("AppBundle:Qualification");
        $existingUser = $this->userRepository->findOneById($user_id);
        $existingUser->setFirstName($user->getFirstName());
        $existingUser->setLastName($user->getLastName());
        $existingUser->setStreet($user->getStreet());
        $existingUser->setStreetNumber($user->getStreetNumber());
        $existingUser->setZip($user->getZip());
        $existingUser->setCity($user->getCity());
        $existingUser->setIsGuest($user->getIsGuest());
        $existingUser->setWeight($user->getWeight());
        $existingUser->setIsMember($user->getIsMember());
        $existingUser->setReserve($user->getReserve());
        $existingUser->setLicenseExpiration($user->getLicenseExpiration());
        $existingUser->setLicenseNo($user->getLicenseNo());
        $existingUser->setEmergencyContact($user->getEmergencyContact());
        $existingUser->setEmergencyNumber($user->getEmergencyNumber());
        $existingUser->setDisclaimerDate($user->getDisclaimerDate());
        $existingUser->setIban($user->getIban());
        foreach ($existingUser->getQualifications() as $qualification) {
            $existingUser->removeQualification($qualification);
        }
        foreach ($qualifications as $qualification) {
            $loadedQualification = $qualificationRepository->findOneByName($qualification);
            if ($loadedQualification != null) {
                $existingUser->addQualification($loadedQualification);
            }

        }
        $this->em->merge($existingUser);
        $this->em->flush();

    }

    public function checkIBAN(User $user)

    {
        $validator = Validation::createValidator();
        $constraint = new Collection(array(
            'iban' => array(new Iban(), new NotNull()),
        ));

        // check for violations
        $violations = $validator->validate(array('iban' => $user->getIban()), $constraint);
        return count($violations) == 0;
    }
}