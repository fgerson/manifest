<?php
namespace AppBundle\Dto;
class VoucherDto{
    private $voucherTypeDto;
    private $valid;

    /**
     * @return mixed
     */
    public function getVoucherTypeDto()
    {
        return $this->voucherTypeDto;
    }

    /**
     * @param mixed $voucherTypeDto
     */
    public function setVoucherTypeDto($voucherTypeDto)
    {
        $this->voucherTypeDto = $voucherTypeDto;
    }

    /**
     * @return mixed
     */
    public function getValid()
    {
        return $this->valid;
    }

    /**
     * @param mixed $valid
     */
    public function setValid($valid)
    {
        $this->valid = $valid;
    }


    public static function createInvalidVoucherDto(){
        $voucherDto = new VoucherDto();
        $voucherDto->setValid(false);
        return $voucherDto;
    }
}