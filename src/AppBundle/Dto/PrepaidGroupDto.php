<?php
namespace AppBundle\Dto;
use AppBundle\Entity\PrepaidGroup;

class PrepaidGroupDto{
    private $prepaid_group;
   private $prepaidDtos;
    private $deleteable;

    /**
     * @return mixed
     */
    public function getPrepaidGroup()
    {
        return $this->prepaid_group;
    }

    /**
     * @param mixed $prepaid_block
     */
    public function setPrepaidGroup(PrepaidGroup $prepaid_group)
    {
        $this->prepaid_group = $prepaid_group;
    }






    /**
     * @return mixed
     */
    public function getDeleteable()
    {
        return $this->deleteable;
    }

    /**
     * @param mixed $deleteable
     */
    public function setDeleteable($deleteable)
    {
        $this->deleteable = $deleteable;
    }

    /**
     * @return mixed
     */
    public function getPrepaidDtos()
    {
        return $this->prepaidDtos;
    }

    /**
     * @param mixed $prepaidDtos
     */
    public function addPrepaidDto($prepaidDto)
    {
        $this->prepaidDtos[] = $prepaidDto;
    }


}