<?php
namespace AppBundle\Util;

use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;

class PaginationUtil{

    private $paginator;
    private $query;
    const PAGESIZE = 10;
    public function setQuery($query){
        $this->paginator = new Paginator($query);
        $this->paginator->setUseOutputWalkers(false);
    }

    public function getPagesCount(){
        $cnt =  ceil($this->paginator->count() / PaginationUtil::PAGESIZE);
        if($cnt == 0) $cnt = 1;
        return $cnt;
    }

    public function getPage($page = 1){
        $results = $this->paginator->getQuery()->setFirstResult(PaginationUtil::PAGESIZE * ($page -1))
            ->setMaxResults(PaginationUtil::PAGESIZE)
            ->getResult();
        return $results;
    }
}