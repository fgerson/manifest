<?php
/**
 * Created by PhpStorm.
 * User: Fabian
 * Date: 03.02.2019
 * Time: 01:48
 */

namespace AppBundle\Util;


class TimeUtil
{

    public static function getFirstDayOfYear(){
        $firstDay = new \DateTime();
        $firstDay->setDate($firstDay->format('Y'), 1, 1);
        return $firstDay;
    }

}