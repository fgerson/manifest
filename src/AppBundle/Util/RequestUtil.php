<?php
namespace AppBundle\Util;

class RequestUtil{
    public static function getPostAsJson($content){
        $content= urldecode($content);
      
       $vars = explode('&', $content);
       $post = array();
       foreach($vars as $var){
        $sVar = explode('=', $var);
            if (strpos($sVar[0], '[]') !== false) {
                    $sVar[0] = str_replace('[]', '',$sVar[0] );
                    $post[$sVar[0]][] = $sVar[1];
                
            }else{
                $post[$sVar[0]] = $sVar[1];
            }
        }
      
        return json_encode($post);
    }
}