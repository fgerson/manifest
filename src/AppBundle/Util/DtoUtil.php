<?php

namespace AppBundle\Util;

use AppBundle\Dto\JumpTypeDto;
use AppBundle\Dto\VoucherDto;
use AppBundle\Dto\VoucherTypeDto;
use AppBundle\Entity\Jumptype;
use AppBundle\Entity\Voucher;
use AppBundle\Entity\VoucherType;

class DtoUtil
{


    public static function toJumpTypeDto(Jumptype $jumptype)
    {
        $jumpTypeDto = new JumpTypeDto();
        $jumpTypeDto->setId($jumptype->getId());
        $jumpTypeDto->setName($jumptype->getName());
        return $jumpTypeDto;
    }

    public static function toVoucherTypeDto(VoucherType $voucherType)
    {
        $voucherTypeDto = new VoucherTypeDto();
        $voucherTypeDto->setPrice($voucherType->getPrice());

        return $voucherTypeDto;
    }

    public static function toVoucherDto(Voucher $voucher)
    {
        $voucherDto = new VoucherDto();
        $voucherDto->setVoucherTypeDto(self::toVoucherTypeDto($voucher->getVoucherType()));
        return $voucherDto;
    }
}