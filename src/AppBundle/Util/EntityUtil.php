<?php
namespace AppBundle\Util;

use JMS\Serializer\SerializerBuilder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
class EntityUtil{
  /*  
    public static function denormalizeJson($json, $entity){
       
         $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
           $denormalized =  $serializer->deserialize($json, $entity, 'json');
           return $denormalized;
                    
     }
   
    
     public static function denormalizeArray($array, $entity){
       
         $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
           return $serializer->denormalize($array, $entity);
           
         
       
    }
    
     public static function denormalizeArrayList($arrays, $entity){
       
         $result = array();
         foreach($arrays as $array){
            $result[] = EntityUtil::denormalizeArray($array, $entity);  
         }
         
         return $result;
           
         
       
    }
    public static function denormalizeEntityList($json, $entity){
         $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
          $decoded = $serializer->decode($json, 'json');
          $denormalized = array();
          foreach($decoded as $decodedP){
             $denormalized[] = $serializer->denormalize($decodedP, $entity);
          }
          
           return $denormalized;
    }
    
    public static function getJsonAsArray($json){
         $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
          return $serializer->decode($json, 'json');
    }
    
    */
      public static function getArrayFromJson($json){
       
         $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
           return $serializer->decode($json, 'json');
           
         
    }    
                    
    public static function getJsonFromObject($object){
        $serializer = SerializerBuilder::create()->build();
        return $serializer->serialize($object, 'json');        

    }
    
    public static function getObjectFromJson($json, $type){
        $serializer = SerializerBuilder::create()->build();
        return $serializer->deserialize($json, $type, 'json');
            
    }        
}