<?php
namespace AppBundle\Util;
class SeatCounter{
    public static function countSeats($load){
        $seats = 0;
        foreach($load->getJumpGroups() as $jumpGroup){
          
            foreach($jumpGroup->getSeats() as $seat){
          
            $seats++;
        }
        }
    return $seats;
    }  
}
