<?php
namespace AppBundle\DBAL;
class EnumInvoiceType extends EnumType{
    protected $name = 'enuminvoicetype';
    protected $values = array('invoice', 'credit');
}