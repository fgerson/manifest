<?php
namespace AppBundle\DBAL;
class EnumSalaryType extends EnumType{
    protected $name = 'enumsalarytype';
    protected $values = array('tandemmaster', 'video', 'aff', 'instructor');
}