<?php
namespace AppBundle\DBAL;
class EnumPaymentType extends EnumType{
    protected $name = 'enumpaymenttype';
    protected $values = array('cash', 'sepa', 'transfer');
}