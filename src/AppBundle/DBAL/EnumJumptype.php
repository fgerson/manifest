<?php
namespace AppBundle\DBAL;
class EnumJumptype extends EnumType{
    protected $name = 'enumjumptype';
    protected $values = array('solo', 'tandem', 'aff', 'instructor');
}