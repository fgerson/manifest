<?php
namespace AppBundle\DBAL;
class EnumRentType extends EnumType{
    protected $name = 'enumrenttype';
    protected $values = array('solo', 'tandem');
}