<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 *
 * @ORM\Table(name="rent")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RentRepository")

 
 */
class Rent{
    
    
  
    /** @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @Type("AppBundle\Entity\User")
     */
    private $user;


    /**
     * @ORM\ManyToOne(targetEntity="RentType")
     * @Type("AppBundle\Entity\RentType")
     */
    private $rent_type;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\JumpGroup", inversedBy="rent")
     * @Type("AppBundle\Entity\JumpGroup")
     */
    private $jump_group;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Rent
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set rentType
     *
     * @param \AppBundle\Entity\RentType $rentType
     *
     * @return Rent
     */
    public function setRentType(\AppBundle\Entity\RentType $rentType = null)
    {
        $this->rent_type = $rentType;

        return $this;
    }

    /**
     * Get rentType
     *
     * @return \AppBundle\Entity\RentType
     */
    public function getRentType()
    {
        return $this->rent_type;
    }

  

    /**
     * Set jumpGroup
     *
     * @param \AppBundle\Entity\JumpGroup $jumpGroup
     *
     * @return Rent
     */
    public function setJumpGroup(\AppBundle\Entity\JumpGroup $jumpGroup = null)
    {
        $this->jump_group = $jumpGroup;

        return $this;
    }

    /**
     * Get jumpGroup
     *
     * @return \AppBundle\Entity\JumpGroup
     */
    public function getJumpGroup()
    {
        return $this->jump_group;
    }
}
