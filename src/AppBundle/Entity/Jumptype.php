<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * 
 * @ORM\Table(name="jumptype")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\JumptypeRepository")
 
 */
class Jumptype{
  
  
    /** @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     
     */
    protected $id;
    
    
    
    
    
     /** 
      * @ORM\Column(type="string") 
      * @Type("string") 
     */
    private $name;
    
  
    /** 
      * @ORM\Column(type="decimal") 
      * @Type("decimal") 
     */
   private $price;
   
   
    /** 
      * @ORM\Column(type="integer") 
      * @Type("integer") 
     */
   private $height;
   
    /** 
      * @ORM\Column(type="enumjumptype") 
      * @Type("string") 
     */
    private $jumptype;

    /**
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $member_discount;

    /**
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $js_triggers;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Jumptype
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Jumptype
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set height
     *
     * @param integer $height
     *
     * @return Jumptype
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return integer
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set jumptype
     *
     * @param enumjumptype $jumptype
     *
     * @return Jumptype
     */
    public function setJumptype($jumptype)
    {
        $this->jumptype = $jumptype;

        return $this;
    }

    /**
     * Get jumptype
     *
     * @return enumjumptype
     */
    public function getJumptype()
    {
        return $this->jumptype;
    }

    /**
     * Set jsTriggers
     *
     * @param string $jsTriggers
     *
     * @return Jumptype
     */
    public function setJsTriggers($jsTriggers)
    {
        $this->js_triggers = $jsTriggers;

        return $this;
    }

    /**
     * Get jsTriggers
     *
     * @return string
     */
    public function getJsTriggers()
    {
        return $this->js_triggers;
    }



    /**
     * Set memberDiscount
     *
     * @param integer $memberDiscount
     *
     * @return Jumptype
     */
    public function setMemberDiscount($memberDiscount)
    {
        $this->member_discount = $memberDiscount;

        return $this;
    }

    /**
     * Get memberDiscount
     *
     * @return integer
     */
    public function getMemberDiscount()
    {
        return $this->member_discount;
    }
}
