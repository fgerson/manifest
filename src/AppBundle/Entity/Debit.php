<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 *
 * @ORM\Table(name="debit")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DebitRepository")

 
 */
class Debit{
    
    
  
    /** @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     
     */
    protected $id;

    /**
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime")
     */
    private $created ;

    /**
     * @ORM\Column(type="decimal", scale=2, precision=10)
     * @Type("decimal")
     */
    private $sum = 0;

    /**
     * @ORM\ManyToOne(targetEntity="LoginUser")
     * @Type("AppBundle\Entity\LoginUser")
     */
    private $loginUser;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Debit
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set sum
     *
     * @param string $sum
     *
     * @return Debit
     */
    public function setSum($sum)
    {
        $this->sum = $sum;

        return $this;
    }

    /**
     * Get sum
     *
     * @return string
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * Set loginUser
     *
     * @param \AppBundle\Entity\LoginUser $loginUser
     *
     * @return Debit
     */
    public function setLoginUser(\AppBundle\Entity\LoginUser $loginUser = null)
    {
        $this->loginUser = $loginUser;

        return $this;
    }

    /**
     * Get loginUser
     *
     * @return \AppBundle\Entity\LoginUser
     */
    public function getLoginUser()
    {
        return $this->loginUser;
    }
}
