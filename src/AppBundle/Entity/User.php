<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User{
    
    /** @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     
     */
    private $id;
    
     /**
      * @ORM\Column(type="string")
      * @Type("string")
     */
    private $first_name;
    
     /**
      * @ORM\Column(type="string")
      * @Type("string")
     */
    private $last_name;

    /**
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $street;

    /**
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $street_number;

    /**
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $zip;

    /**
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $city;

    /**
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $license_no;

    /**
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $emergency_contact;
    /**
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $emergency_number;

    /**
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $iban;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Type("datetime")
     */
    private $reserve = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Type("datetime")
     */
    private $disclaimer_date = null ;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Type("datetime")
     */
    private $license_expiration = null;

    /**
     * @ORM\ManyToMany(targetEntity="Qualification")
     * @Type("array<AppBundle\Entity\Qualification>")
     *
     */
    private $qualifications;

    /**
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $isGuest = 0;

    /**
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $isMember = 0;

    /**
     * @ORM\Column(type="integer")
     * @Type("int")
     */
    private $weight = 0;

     /**
     * @ORM\Column(type="integer")
      * @Type("int")
     */
    private $deleted = 0;

   

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     *
     * @return User
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return User
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set streetNumber
     *
     * @param string $streetNumber
     *
     * @return User
     */
    public function setStreetNumber($streetNumber)
    {
        $this->street_number = $streetNumber;

        return $this;
    }

    /**
     * Get streetNumber
     *
     * @return string
     */
    public function getStreetNumber()
    {
        return $this->street_number;
    }

    /**
     * Set zip
     *
     * @param string $zip
     *
     * @return User
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->qualifications = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add qualification
     *
     * @param \AppBundle\Entity\Qualification $qualification
     *
     * @return User
     */
    public function addQualification(\AppBundle\Entity\Qualification $qualification)
    {
        $this->qualifications[] = $qualification;

        return $this;
    }

    /**
     * Remove qualification
     *
     * @param \AppBundle\Entity\Qualification $qualification
     */
    public function removeQualification(\AppBundle\Entity\Qualification $qualification)
    {
        $this->qualifications->removeElement($qualification);
    }

    /**
     * Get qualifications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQualifications()
    {
        return $this->qualifications;
    }

    /**
     * Set isGuest
     *
     * @param boolean $isGuest
     *
     * @return User
     */
    public function setIsGuest($isGuest)
    {
        $this->isGuest = $isGuest;

        return $this;
    }

    /**
     * Get isGuest
     *
     * @return boolean
     */
    public function getIsGuest()
    {
        return $this->isGuest;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return User
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set isMember
     *
     * @param boolean $isMember
     *
     * @return User
     */
    public function setIsMember($isMember)
    {
        $this->isMember = $isMember;

        return $this;
    }

    /**
     * Get isMember
     *
     * @return boolean
     */
    public function getIsMember()
    {
        return $this->isMember;
    }

    /**
     * Set reserve
     *
     * @param \DateTime $reserve
     *
     * @return User
     */
    public function setReserve($reserve)
    {
        $this->reserve = $reserve;

        return $this;
    }

    /**
     * Get reserve
     *
     * @return \DateTime
     */
    public function getReserve()
    {
        return $this->reserve;
    }

  

    /**
     * Set licenseExpiration
     *
     * @param \DateTime $licenseExpiration
     *
     * @return User
     */
    public function setLicenseExpiration($licenseExpiration)
    {
        $this->license_expiration = $licenseExpiration;

        return $this;
    }

    /**
     * Get licenseExpiration
     *
     * @return \DateTime
     */
    public function getLicenseExpiration()
    {
        return $this->license_expiration;
    }

    /**
     * Set licenseNo
     *
     * @param string $licenseNo
     *
     * @return User
     */
    public function setLicenseNo($licenseNo)
    {
        $this->license_no = $licenseNo;

        return $this;
    }

    /**
     * Get licenseNo
     *
     * @return string
     */
    public function getLicenseNo()
    {
        return $this->license_no;
    }

    /**
     * Set iban
     *
     * @param string $iban
     *
     * @return User
     */
    public function setIban($iban)
    {
        $this->iban = $iban;

        return $this;
    }

    /**
     * Get iban
     *
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Set emergencyContact
     *
     * @param string $emergencyContact
     *
     * @return User
     */
    public function setEmergencyContact($emergencyContact)
    {
        $this->emergency_contact = $emergencyContact;

        return $this;
    }

    /**
     * Get emergencyContact
     *
     * @return string
     */
    public function getEmergencyContact()
    {
        return $this->emergency_contact;
    }

    /**
     * Set emergencyNumber
     *
     * @param string $emergencyNumber
     *
     * @return User
     */
    public function setEmergencyNumber($emergencyNumber)
    {
        $this->emergency_number = $emergencyNumber;

        return $this;
    }

    /**
     * Get emergencyNumber
     *
     * @return string
     */
    public function getEmergencyNumber()
    {
        return $this->emergency_number;
    }

    /**
     * Set disclaimerDate
     *
     * @param \DateTime $disclaimerDate
     *
     * @return User
     */
    public function setDisclaimerDate($disclaimerDate)
    {
        $this->disclaimer_date = $disclaimerDate;

        return $this;
    }

    /**
     * Get disclaimerDate
     *
     * @return \DateTime
     */
    public function getDisclaimerDate()
    {
        return $this->disclaimer_date;
    }
}
