<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 *
 * @ORM\Table(name="prepaid_type_group")
 * @ORM\Entity
 */
class PrepaidTypeGroup
{


    /** @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $name;




    /**
     * @ORM\Column(type="decimal", scale=2, precision=10)
     * @Type("decimal")
     */
    private $price = 0;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PrepaidType", mappedBy="prepaidTypeGroup")
     * @Type("array<AppBundle\Entity\PrepaidType>")
     *
     */
    private $prepaidTypes;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PrepaidTypeGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return PrepaidTypeGroup
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->prepaidTypes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add prepaidType
     *
     * @param \AppBundle\Entity\PrepaidType $prepaidType
     *
     * @return PrepaidTypeGroup
     */
    public function addPrepaidType(\AppBundle\Entity\PrepaidType $prepaidType)
    {
        $this->prepaidTypes[] = $prepaidType;

        return $this;
    }

    /**
     * Remove prepaidType
     *
     * @param \AppBundle\Entity\PrepaidType $prepaidType
     */
    public function removePrepaidType(\AppBundle\Entity\PrepaidType $prepaidType)
    {
        $this->prepaidTypes->removeElement($prepaidType);
    }

    /**
     * Get prepaidTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrepaidTypes()
    {
        return $this->prepaidTypes;
    }
}
