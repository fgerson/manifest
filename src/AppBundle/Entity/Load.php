<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * @ORM\Table(name="loads")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LoadRepository")
 */
class Load{
     const MAX_SEATS = 10;
    /** @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     
     */
    private $id;
    
     /**
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime")
     */
    private $created ;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Type("datetime")
     */
    private $scheduled_start = null;


    /**
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $shouldFollow = false;

     /**
     * @ORM\Column(type="datetime", nullable=true)
      * @Type("datetime")
     */
    private $started = null;

   /**
     * @ORM\OneToMany(targetEntity="JumpGroup", mappedBy="load")     
     * @Type("array<AppBundle\Entity\JumpGroup>")
     * 
     */
    private $jumpGroups;

    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->jumpGroups = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Load
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set started
     *
     * @param \DateTime $started
     *
     * @return Load
     */
    public function setStarted($started)
    {
        $this->started = $started;

        return $this;
    }

    /**
     * Get started
     *
     * @return \DateTime
     */
    public function getStarted()
    {
        return $this->started;
    }

    /**
     * Add jumpGroup
     *
     * @param \AppBundle\Entity\JumpGroup $jumpGroup
     *
     * @return Load
     */
    public function addJumpGroup(\AppBundle\Entity\JumpGroup $jumpGroup)
    {
        $this->jumpGroups[] = $jumpGroup;

        return $this;
    }

    /**
     * Remove jumpGroup
     *
     * @param \AppBundle\Entity\JumpGroup $jumpGroup
     */
    public function removeJumpGroup(\AppBundle\Entity\JumpGroup $jumpGroup)
    {
        $this->jumpGroups->removeElement($jumpGroup);
    }

    /**
     * Get jumpGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJumpGroups()
    {
        return $this->jumpGroups;
    }

    /**
     * Set scheduledStart
     *
     * @param \DateTime $scheduledStart
     *
     * @return Load
     */
    public function setScheduledStart($scheduledStart)
    {
        $this->scheduled_start = $scheduledStart;

        return $this;
    }

    /**
     * Get scheduledStart
     *
     * @return \DateTime
     */
    public function getScheduledStart()
    {
        return $this->scheduled_start;
    }

    /**
     * Set shouldFollow
     *
     * @param boolean $shouldFollow
     *
     * @return Load
     */
    public function setShouldFollow($shouldFollow)
    {
        $this->shouldFollow = $shouldFollow;

        return $this;
    }

    /**
     * Get shouldFollow
     *
     * @return boolean
     */
    public function getShouldFollow()
    {
        return $this->shouldFollow;
    }
}
