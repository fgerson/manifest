<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 *
 * @ORM\Table(name="prepaid")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PrepaidRepository")

 
 */
class Prepaid{
    
    
  
    /** @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @Type("AppBundle\Entity\User")
     */
    private $user;


    /**
     * @ORM\ManyToOne(targetEntity="PrepaidType")
     * @Type("AppBundle\Entity\PrepaidType")
     */
    private $prepaid_type;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PrepaidGroup", inversedBy="prepaids")
     * @Type("AppBundle\Entity\PrepaidGroup")
     */
    private $prepaid_group;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Prepaid
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set prepaidType
     *
     * @param \AppBundle\Entity\PrepaidType $prepaidType
     *
     * @return Prepaid
     */
    public function setPrepaidType(\AppBundle\Entity\PrepaidType $prepaidType = null)
    {
        $this->prepaid_type = $prepaidType;

        return $this;
    }

    /**
     * Get prepaidType
     *
     * @return \AppBundle\Entity\PrepaidType
     */
    public function getPrepaidType()
    {
        return $this->prepaid_type;
    }

    /**
     * Set prepaidGroup
     *
     * @param \AppBundle\Entity\PrepaidGroup $prepaidGroup
     *
     * @return Prepaid
     */
    public function setPrepaidGroup(\AppBundle\Entity\PrepaidGroup $prepaidGroup = null)
    {
        $this->prepaid_group = $prepaidGroup;

        return $this;
    }

    /**
     * Get prepaidGroup
     *
     * @return \AppBundle\Entity\PrepaidGroup
     */
    public function getPrepaidGroup()
    {
        return $this->prepaid_group;
    }
}
