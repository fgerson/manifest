<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * 
 * @ORM\Table(name="salary_type")
 * @ORM\Entity
 
 */
class SalaryType{
    
    
  
    /** @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     
     */
    protected $id;
    
    
   
    
    
     /** 
      * @ORM\Column(type="string") 
      * @Type("string") 
     */
    private $name;
    
  
    /** 
      * @ORM\Column(type="decimal") 
      * @Type("decimal") 
     */
   private $credit;
   
   
 
     /** 
      * @ORM\Column(type="enumsalarytype") 
      * @Type("string") 
     */
    private $salaryType;
   
   
  
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SalaryType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set credit
     *
     * @param string $credit
     *
     * @return SalaryType
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;

        return $this;
    }

    /**
     * Get credit
     *
     * @return string
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * Set salaryType
     *
     * @param enumsalarytype $salaryType
     *
     * @return SalaryType
     */
    public function setSalaryType($salaryType)
    {
        $this->salaryType = $salaryType;

        return $this;
    }

    /**
     * Get salaryType
     *
     * @return enumsalarytype
     */
    public function getSalaryType()
    {
        return $this->salaryType;
    }
}
