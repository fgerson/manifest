<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * 
 * @ORM\Table(name="jumpgroup")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\JumpGroupRepository")

 
 */
class JumpGroup
{


    /** @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="Load", inversedBy="jumpGroups")
     * @Type("AppBundle\Entity\Load")
     */
    private $load;

    /**
     * @ORM\ManyToOne(targetEntity="Jumptype")
     * @Type("AppBundle\Entity\Jumptype")
     */
    private $jumpType;

    /**
     * @ORM\OneToMany(targetEntity="Seat", mappedBy="jumpGroup")
     * @Type("AppBundle\Entity\Seat")
     */
    private $seats;


    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @Type("AppBundle\Entity\User")
     */
    private $payer;


/**
* @ORM\OneToOne(targetEntity="AppBundle\Entity\Rent", mappedBy="jump_group")
* @Type("AppBundle\Entity\Rent")
*/
    private $rent;
    /**
     * @ORM\ManyToOne(targetEntity="Invoice")
     * @Type("AppBundle\Entity\Invoice")
     */
    private $invoice;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Voucher", inversedBy="jumpGroup")
     * @Type("AppBundle\Entity\Voucher")
     */
    private $voucher;

    /**
     * @ORM\ManyToOne(targetEntity="Prepaid")
     * @Type("AppBundle\Entity\Prepaid")
     */
    private $prepaid = null;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->seats = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set load
     *
     * @param \AppBundle\Entity\Load $load
     *
     * @return JumpGroup
     */
    public function setLoad(\AppBundle\Entity\Load $load = null)
    {
        $this->load = $load;

        return $this;
    }

    /**
     * Get load
     *
     * @return \AppBundle\Entity\Load
     */
    public function getLoad()
    {
        return $this->load;
    }

    /**
     * Set jumpType
     *
     * @param \AppBundle\Entity\Jumptype $jumpType
     *
     * @return JumpGroup
     */
    public function setJumpType(\AppBundle\Entity\Jumptype $jumpType = null)
    {
        $this->jumpType = $jumpType;

        return $this;
    }

    /**
     * Get jumpType
     *
     * @return \AppBundle\Entity\Jumptype
     */
    public function getJumpType()
    {
        return $this->jumpType;
    }

    /**
     * Add seat
     *
     * @param \AppBundle\Entity\Seat $seat
     *
     * @return JumpGroup
     */
    public function addSeat(\AppBundle\Entity\Seat $seat)
    {
        $this->seats[] = $seat;

        return $this;
    }

    /**
     * Remove seat
     *
     * @param \AppBundle\Entity\Seat $seat
     */
    public function removeSeat(\AppBundle\Entity\Seat $seat)
    {
        $this->seats->removeElement($seat);
    }

    /**
     * Get seats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSeats()
    {
        return $this->seats;
    }

    /**
     * Set payer
     *
     * @param \AppBundle\Entity\User $payer
     *
     * @return JumpGroup
     */
    public function setPayer(\AppBundle\Entity\User $payer = null)
    {
        $this->payer = $payer;

        return $this;
    }

    /**
     * Get payer
     *
     * @return \AppBundle\Entity\User
     */
    public function getPayer()
    {
        return $this->payer;
    }

    /**
     * Set invoice
     *
     * @param \AppBundle\Entity\Invoice $invoice
     *
     * @return JumpGroup
     */
    public function setInvoice(\AppBundle\Entity\Invoice $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return \AppBundle\Entity\Invoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set prepaid
     *
     * @param \AppBundle\Entity\Prepaid $prepaid
     *
     * @return JumpGroup
     */
    public function setPrepaid(\AppBundle\Entity\Prepaid $prepaid = null)
    {
        $this->prepaid = $prepaid;

        return $this;
    }

    /**
     * Get prepaid
     *
     * @return \AppBundle\Entity\Prepaid
     */
    public function getPrepaid()
    {
        return $this->prepaid;
    }

    /**
     * Set voucher
     *
     * @param \AppBundle\Entity\Voucher $voucher
     *
     * @return JumpGroup
     */
    public function setVoucher(\AppBundle\Entity\Voucher $voucher = null)
    {
        $this->voucher = $voucher;

        return $this;
    }

    /**
     * Get voucher
     *
     * @return \AppBundle\Entity\Voucher
     */
    public function getVoucher()
    {
        return $this->voucher;
    }

   

    /**
     * Set rent
     *
     * @param \AppBundle\Entity\Rent $rent
     *
     * @return JumpGroup
     */
    public function setRent(\AppBundle\Entity\Rent $rent = null)
    {
        $this->rent = $rent;

        return $this;
    }

    /**
     * Get rent
     *
     * @return \AppBundle\Entity\Rent
     */
    public function getRent()
    {
        return $this->rent;
    }
}
