<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * 
 * @ORM\Table(name="invoice")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InvoiceRepository")

 
 */
class Invoice{
    
    
  
    /** @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $invoice_number;

    /**
     *
     * @ORM\Column(type="blob", nullable=true)
     * @Type("string")
     */
    private $pdf ;

    /**
     * @ORM\Column(type="decimal", scale=2)
     * @Type("decimal")
     */
    private $sum;

    /**
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime")
     */
    private $created ;

    /**
     * @ORM\Column(type="enumpaymenttype")
     * @Type("string")
     */
    private $paymentType;

    /**
     * @ORM\ManyToOne(targetEntity="User")

     * @Type("AppBundle\Entity\User")
     */
    private $payer;
    /**
     * @ORM\Column(type="enuminvoicetype")
     * @Type("string")
     */
    private $invoice_type;
    /**
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $cleared = 0;

    /**
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $ust = 0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pdf
     *
     * @param string $pdf
     *
     * @return Invoice
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * Get pdf
     *
     * @return string
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set cleared
     *
     * @param boolean $cleared
     *
     * @return Invoice
     */
    public function setCleared($cleared)
    {
        $this->cleared = $cleared;

        return $this;
    }

    /**
     * Get cleared
     *
     * @return boolean
     */
    public function getCleared()
    {
        return $this->cleared;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Invoice
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set sum
     *
     * @param string $sum
     *
     * @return Invoice
     */
    public function setSum($sum)
    {
        $this->sum = $sum;

        return $this;
    }

    /**
     * Get sum
     *
     * @return string
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * Set invoiceNumber
     *
     * @param string $invoiceNumber
     *
     * @return Invoice
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        $this->invoice_number = $invoiceNumber;

        return $this;
    }

    /**
     * Get invoiceNumber
     *
     * @return string
     */
    public function getInvoiceNumber()
    {
        return $this->invoice_number;
    }

    /**
     * Set paymentType
     *
     * @param enumpaymenttype $paymentType
     *
     * @return Invoice
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * Get paymentType
     *
     * @return enumpaymenttype
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * Set invoiceType
     *
     * @param enuminvoicetype $invoiceType
     *
     * @return Invoice
     */
    public function setInvoiceType($invoiceType)
    {
        $this->invoice_type = $invoiceType;

        return $this;
    }

    /**
     * Get invoiceType
     *
     * @return enuminvoicetype
     */
    public function getInvoiceType()
    {
        return $this->invoice_type;
    }

    /**
     * Set payer
     *
     * @param \AppBundle\Entity\User $payer
     *
     * @return Invoice
     */
    public function setPayer(\AppBundle\Entity\User $payer = null)
    {
        $this->payer = $payer;

        return $this;
    }

    /**
     * Get payer
     *
     * @return \AppBundle\Entity\User
     */
    public function getPayer()
    {
        return $this->payer;
    }

    /**
     * Set ust
     *
     * @param boolean $ust
     *
     * @return Invoice
     */
    public function setUst($ust)
    {
        $this->ust = $ust;

        return $this;
    }

    /**
     * Get ust
     *
     * @return boolean
     */
    public function getUst()
    {
        return $this->ust;
    }
}
