<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 *
 * @ORM\Table(name="rent_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RentTypeRepository")

 
 */
class RentType{
    
    
  
    /** @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     
     */
    protected $id;



    /**
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $name;

    /**
     * @ORM\Column(type="decimal", scale=2)
     * @Type("decimal")
     */
    private $price;

    /**
     * @ORM\Column(type="enumrenttype")
     * @Type("string")
     */
    private $rentType;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Rent
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Rent
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set rentType
     *
     * @param enumrenttype $rentType
     *
     * @return RentType
     */
    public function setRentType($rentType)
    {
        $this->rentType = $rentType;

        return $this;
    }

    /**
     * Get rentType
     *
     * @return enumrenttype
     */
    public function getRentType()
    {
        return $this->rentType;
    }
}
