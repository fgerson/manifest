<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 *
 * @ORM\Table(name="prepaid_group")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PrepaidGroupRepository")

 
 */
class PrepaidGroup{
    
    
  
    /** @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @Type("AppBundle\Entity\User")
     */
    private $payer;




    /**
    * @ORM\ManyToOne(targetEntity="Invoice")
    * @Type("AppBundle\Entity\Invoice")
    */
    private $invoice;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Type("datetime")
     */
    private $buyedAt ;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PrepaidTypeGroup")
     * @Type("AppBundle\Entity\PrepaidTypeGroup")
     */
    private $prepaidTypeGroup;

    

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Prepaid", mappedBy="prepaid_group")
     * @Type("AppBundle\Entity\Prepaid")
     */
    private $prepaids;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set payer
     *
     * @param \AppBundle\Entity\User $payer
     *
     * @return PrepaidGroup
     */
    public function setPayer(\AppBundle\Entity\User $payer = null)
    {
        $this->payer = $payer;

        return $this;
    }

    /**
     * Get payer
     *
     * @return \AppBundle\Entity\User
     */
    public function getPayer()
    {
        return $this->payer;
    }

    /**
     * Set invoice
     *
     * @param \AppBundle\Entity\Invoice $invoice
     *
     * @return PrepaidGroup
     */
    public function setInvoice(\AppBundle\Entity\Invoice $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return \AppBundle\Entity\Invoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set prepaidTypeGroup
     *
     * @param \AppBundle\Entity\PrepaidTypeGroup $prepaidTypeGroup
     *
     * @return PrepaidGroup
     */
    public function setPrepaidTypeGroup(\AppBundle\Entity\PrepaidTypeGroup $prepaidTypeGroup = null)
    {
        $this->prepaidTypeGroup = $prepaidTypeGroup;

        return $this;
    }

    /**
     * Get prepaidTypeGroup
     *
     * @return \AppBundle\Entity\PrepaidTypeGroup
     */
    public function getPrepaidTypeGroup()
    {
        return $this->prepaidTypeGroup;
    }

    /**
     * Set buyedAt
     *
     * @param \DateTime $buyedAt
     *
     * @return PrepaidGroup
     */
    public function setBuyedAt($buyedAt)
    {
        $this->buyedAt = $buyedAt;

        return $this;
    }

    /**
     * Get buyedAt
     *
     * @return \DateTime
     */
    public function getBuyedAt()
    {
        return $this->buyedAt;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->prepaids = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add prepaid
     *
     * @param \AppBundle\Entity\Prepaid $prepaid
     *
     * @return PrepaidGroup
     */
    public function addPrepaid(\AppBundle\Entity\Prepaid $prepaid)
    {
        $this->prepaids[] = $prepaid;

        return $this;
    }

    /**
     * Remove prepaid
     *
     * @param \AppBundle\Entity\Prepaid $prepaid
     */
    public function removePrepaid(\AppBundle\Entity\Prepaid $prepaid)
    {
        $this->prepaids->removeElement($prepaid);
    }

    /**
     * Get prepaids
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrepaids()
    {
        return $this->prepaids;
    }


}
