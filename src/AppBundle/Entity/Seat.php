<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * 
 * @ORM\Table(name="seat")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SeatRepository")

 
 */
class Seat{
    
    
  
    /** @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     
     */
    protected $id;
    
    
      /** 
   * @ORM\ManyToOne(targetEntity="User")
   
     * @Type("AppBundle\Entity\User") 
    */
    private $jumper; 
    
     /** 
   * @ORM\ManyToOne(targetEntity="JumpGroup", inversedBy="seats")
   
     * @Type("AppBundle\Entity\JumpGroup") 
    */
    private $jumpGroup; 
  
      /** 
   * @ORM\ManyToOne(targetEntity="SalaryType")
   
     * @Type("AppBundle\Entity\SalaryType") 
    */
    private $salaryType;

    /**
     * @ORM\ManyToOne(targetEntity="Invoice")
     * @Type("AppBundle\Entity\Invoice")
     */
    private $credit;
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

  
    /**
     * Set jumper
     *
     * @param \AppBundle\Entity\User $jumper
     *
     * @return Seat
     */
    public function setJumper(\AppBundle\Entity\User $jumper = null)
    {
        $this->jumper = $jumper;

        return $this;
    }

    /**
     * Get jumper
     *
     * @return \AppBundle\Entity\User
     */
    public function getJumper()
    {
        return $this->jumper;
    }

    /**
     * Set jumpGroup
     *
     * @param \AppBundle\Entity\JumpGroup $jumpGroup
     *
     * @return Seat
     */
    public function setJumpGroup(\AppBundle\Entity\JumpGroup $jumpGroup = null)
    {
        $this->jumpGroup = $jumpGroup;

        return $this;
    }

    /**
     * Get jumpGroup
     *
     * @return \AppBundle\Entity\JumpGroup
     */
    public function getJumpGroup()
    {
        return $this->jumpGroup;
    }

    /**
     * Set salaryType
     *
     * @param \AppBundle\Entity\SalaryType $salaryType
     *
     * @return Seat
     */
    public function setSalaryType(\AppBundle\Entity\SalaryType $salaryType = null)
    {
        $this->salaryType = $salaryType;

        return $this;
    }

    /**
     * Get salaryType
     *
     * @return \AppBundle\Entity\SalaryType
     */
    public function getSalaryType()
    {
        return $this->salaryType;
    }

    /**
     * Set credit
     *
     * @param \AppBundle\Entity\Credit $credit
     *
     * @return Seat
     */
    public function setCredit(\AppBundle\Entity\Invoice $credit = null)
    {
        $this->credit = $credit;

        return $this;
    }

    /**
     * Get credit
     *
     * @return \AppBundle\Entity\Credit
     */
    public function getCredit()
    {
        return $this->credit;
    }
}
