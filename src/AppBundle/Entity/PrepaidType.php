<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 *
 * @ORM\Table(name="prepaid_type")
 * @ORM\Entity

 
 */
class PrepaidType{
    
    
  
    /** @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $quantity;


    /**
     * @ORM\ManyToOne(targetEntity="Jumptype")
     * @Type("AppBundle\Entity\Jumptype")
     */
    private $jumpType;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PrepaidTypeGroup", inversedBy="prepaidTypes")
     * @Type("AppBundle\Entity\Jumptype")
     */
    private $prepaidTypeGroup;




   
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PrepaidType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return PrepaidType
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set jumpType
     *
     * @param \AppBundle\Entity\Jumptype $jumpType
     *
     * @return PrepaidType
     */
    public function setJumpType(\AppBundle\Entity\Jumptype $jumpType = null)
    {
        $this->jumpType = $jumpType;

        return $this;
    }

    /**
     * Get jumpType
     *
     * @return \AppBundle\Entity\Jumptype
     */
    public function getJumpType()
    {
        return $this->jumpType;
    }

    /**
     * Set prepaidTypeGroup
     *
     * @param \AppBundle\Entity\PrepaidTypeGroup $prepaidTypeGroup
     *
     * @return PrepaidType
     */
    public function setPrepaidTypeGroup(\AppBundle\Entity\PrepaidTypeGroup $prepaidTypeGroup = null)
    {
        $this->prepaidTypeGroup = $prepaidTypeGroup;

        return $this;
    }

    /**
     * Get prepaidTypeGroup
     *
     * @return \AppBundle\Entity\PrepaidTypeGroup
     */
    public function getPrepaidTypeGroup()
    {
        return $this->prepaidTypeGroup;
    }
}
