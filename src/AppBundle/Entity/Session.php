<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\Type;

/**
 * @ORM\Table(name="session")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SessionRepository")
 */
class Session{
    
    /** @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
      * @Type("int")
     */
    private $id;
    
     /**
   
     * @var int
     * @ORM\ManyToOne(targetEntity="LoginUser")
     * @Type("AppBundle\Entity\LoginUser")
     */
    private $user;
    
     /**
      * @ORM\Column(type="string")
      * @Type("string")
     */
    private $sessionKey;


    /**
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime")
     */
    private $lastUsed ;

    /**
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $ip = "-";


    /**
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $deleted = false;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sessionKey
     *
     * @param string $sessionKey
     *
     * @return Session
     */
    public function setSessionKey($sessionKey)
    {
        $this->sessionKey = $sessionKey;

        return $this;
    }

    /**
     * Get sessionKey
     *
     * @return string
     */
    public function getSessionKey()
    {
        return $this->sessionKey;
    }

    /**
     * Set lastUsed
     *
     * @param \DateTime $lastUsed
     *
     * @return Session
     */
    public function setLastUsed($lastUsed)
    {
        $this->lastUsed = $lastUsed;

        return $this;
    }

    /**
     * Get lastUsed
     *
     * @return \DateTime
     */
    public function getLastUsed()
    {
        return $this->lastUsed;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return Session
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Session
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\LoginUser $user
     *
     * @return Session
     */
    public function setUser(\AppBundle\Entity\LoginUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\LoginUser
     */
    public function getUser()
    {
        return $this->user;
    }
}
