<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Util\RequestUtil;
use AppBundle\Util\EntityUtil;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;

class PublicController extends ParentController
{
    
    private $loadRepository;

    
    public function __construct(\AppBundle\Repository\LoadRepository $loadRepository

){
        $this->loadRepository = $loadRepository;

    }

    /**

     * @Route("/public")

     */
    public function overview()
    {
        $loads = $this->loadRepository->getLoadsOfDateAndNotStarted(new \DateTime());
        return new Response($this->renderView("public/load_overview.html.twig", array("loads" => $loads)));
    }


}
