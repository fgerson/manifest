<?php

namespace AppBundle\Controller\Api;

use AppBundle\Controller\ParentController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Util\RequestUtil;
use AppBundle\Util\EntityUtil;
use AppBundle\Entity\User;
use AppBundle\Entity\Load;

class JumpTypeController extends ParentController
{


    private $prepaidService;
    private $userRepository;
    private $jumptypeRepository;

    public function __construct(
        \AppBundle\Repository\JumptypeRepository $jumptypeRepository,
        \AppBundle\Repository\UserRepository $userRepository,
        \AppBundle\Service\PrepaidService $prepaidService
    )
    {

        $this->jumptypeRepository = $jumptypeRepository;
        $this->userRepository = $userRepository;
        $this->prepaidService = $prepaidService;

    }


    /**
     * @Route("prepaid/capacity/{jumper_id}/{jumptype_id}")
     */
    public function getCapacity($jumper_id, $jumptype_id)
    {
        $user = $this->userRepository->findOneById($jumper_id);
        $jumptype = $this->jumptypeRepository->findOneById($jumptype_id);
       $capacity =  $this->prepaidService->getRemainingPrepaids($user, $jumptype);
      return new Response($capacity);
        
        
    }


}
