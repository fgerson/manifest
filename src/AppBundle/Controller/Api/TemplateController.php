<?php

namespace AppBundle\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Util\RequestUtil;
use AppBundle\Util\EntityUtil;
use AppBundle\Entity\User;
use AppBundle\Entity\Load;
class TemplateController extends Controller
{
    
   
   
    
    /**
     * @Route("templates/seats/{template}/{load_id}")
     */
    public function create($template, $load_id)
    {
       return $this->render('templates/'.$template.".html", array("load_id" => $load_id));
        
        
    }
    
    
}
