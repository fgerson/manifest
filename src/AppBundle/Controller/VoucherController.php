<?php

namespace AppBundle\Controller;

use AppBundle\Dto\PrepaidDto;
use AppBundle\Dto\PrepaidGroupDto;
use AppBundle\Dto\VoucherDto;
use AppBundle\Entity\Debit;
use AppBundle\Entity\Prepaid;
use AppBundle\Entity\PrepaidGroup;
use AppBundle\Entity\Voucher;
use AppBundle\Repository\PrepaidRepository;
use AppBundle\Service\PrepaidService;
use AppBundle\Util\DtoUtil;
use AppBundle\Util\HashUtil;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use AppBundle\Util\RequestUtil;
use AppBundle\Util\EntityUtil;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Exception\BadProcessException;

class VoucherController extends ParentController
{


    private $prepaidService;
    private $prepaidRepository;

    public function __construct(


        PrepaidService $prepaidService,
        PrepaidRepository $prepaidRepository


    )
    {


        $this->prepaidService = $prepaidService;
        $this->prepaidRepository = $prepaidRepository;

    }
    /**
     *  @Route("/voucher/overview/{user_id}/{page}")
     * @Route("/voucher/overview/{page}")
     */
    public function overview($user_id = null, $page = 1)
    {
        $this->forceLogin();
        $voucherRepository = $this->getDoctrine()->getRepository("AppBundle:Voucher");
        $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");
        if ($user_id != null) {
            $user = $userRepository->findOneById($user_id);
            $pagination = $voucherRepository->findByUserPaged($user);
        } else {
            $user = null;
            $pagination = $voucherRepository->findAllPaged();
        }
        return $this->render("voucher/overview.html.twig", array(
            "page" => $page,
            "pages" => $pagination->getPagesCount(),
            "vouchers" => $pagination->getPage($page ),
            "jumpers" => $userRepository->findAll(),
            "jumper" => $user

        ));

    }

    /**
     * @Route("/voucher/create")
     */
    public function create(Request $request)
    {
        $this->forceLogin();
        $voucherRepository = $this->getDoctrine()->getRepository("AppBundle:Voucher");
        $voucherTypeRepository = $this->getDoctrine()->getRepository("AppBundle:VoucherType");
        $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");
        if($request->getContent() == null){
            $availableJumpers = $userRepository->findAll();

            return $this->render("voucher/buy.html.twig", array(
                "voucherTypes" => $voucherRepository->findAllVoucherTypes(),
                "users" => $availableJumpers
            ));
        }else{
            $post = EntityUtil::getArrayFromJson(RequestUtil::getPostAsJson($request->getContent()));
            $voucherType = $voucherTypeRepository->findOneById($post['voucher']);
            $voucher = new Voucher();
            $voucher->setVoucherType($voucherType);
            $voucher->setBuyedAt(new \DateTime());
            $voucher->setBuyer($userRepository->findOneById($post['jumper_id']));
            $code = null;
            while($code == null){
                $code = rand(100000000,999999999);
                if($voucherRepository->findOneByCode($code) != null){
                    $code = null;
                }

            }
            $voucher->setCode($code);
            $em = $this->getDoctrine()->getManager();
            $em->persist($voucher);
            $em->flush();
            return $this->redirect("/voucher/overview");
        }


    }


    /**
     * @Route("/voucher/validate/{code}")
     */
    public function validate($code)
    {
        $this->forceLogin();
        $voucherRepository = $this->getDoctrine()->getRepository("AppBundle:Voucher");

        $voucher  = $voucherRepository->findOneByCode($code);

        if($voucher == null || $voucherRepository->isUsed($voucher)){
            $voucherDto = VoucherDto::createInvalidVoucherDto();
        }else{
            $voucherDto = DtoUtil::toVoucherDto($voucher);
            $voucherDto->setValid(true);
        }
        return new Response(EntityUtil::getJsonFromObject($voucherDto));

    }

    /**
     * @Route("/voucher/delete/{voucher_id}")
     */
    public function delete($voucher_id)
    {
        $this->forceLogin();
        $voucherRepository = $this->getDoctrine()->getRepository("AppBundle:Voucher");

        $voucher  = $voucherRepository->findOneById($voucher_id);

        if($voucher->getInvoice() != null || $voucherRepository->isUsed($voucher)){
            throw new BadProcessException();
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($voucher);
        $em->flush();


        return $this->redirect("/voucher/overview");

    }

}
