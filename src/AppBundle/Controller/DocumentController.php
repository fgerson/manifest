<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Debit;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Util\RequestUtil;
use AppBundle\Util\EntityUtil;
use AppBundle\Entity\User;
use AppBundle\Entity\Load;
use AppBundle\Entity\JumpGroup;
use AppBundle\Entity\Seat;
use AppBundle\Entity\Invoice;
use AppBundle\Exception\BadProcessException;


class DocumentController extends ParentController
{


    private $jumpGroupRepository;
    private $seatRepository;
    private $prepaidService;
    const UST = "0.19";


    public function __construct(
        \AppBundle\Repository\JumpGroupRepository $jumpGroupRepository,
        \AppBundle\Repository\SeatRepository $seatRepository,
        \AppBundle\Service\PrepaidService $prepaidService
    )
    {

        $this->jumpGroupRepository = $jumpGroupRepository;
        $this->seatRepository = $seatRepository;
        $this->prepaidService = $prepaidService;

    }


    /**
     * @Route("/invoice/show/{invoice_id}")
     */
    public function showInvoice($invoice_id)
    {
        $currentUser = $this->forceLogin();
        $invoice = $this->getDoctrine()->getRepository("AppBundle:Invoice")->findOneById($invoice_id);
        return new Response(stream_get_contents($invoice->getPdf()),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="out.pdf"'
            )
        );
    }


    /**
     * @Route("/document/payment/unpaid")
     */
    public function unpaid()
    {
        $currentUser = $this->forceLogin();
        $unpaids = $this->jumpGroupRepository->findAllUnpaid();
        $prepaidGroupRepository = $this->getDoctrine()->getRepository("AppBundle:PrepaidGroup");
        $voucherRepository = $this->getDoctrine()->getRepository("AppBundle:Voucher");
        $unpaidUsers = array();
        foreach ($unpaids as $unpaid) {
            if (!in_array($unpaid->getPayer(), $unpaidUsers)) {
                $unpaidUsers[] = $unpaid->getPayer();
            }
        }

        $unpaidGroups = $prepaidGroupRepository->findAllUnpaid();

        foreach ($unpaidGroups as $unpaidGroup) {
            if (!in_array($unpaidGroup->getPayer(), $unpaidUsers)) {
                $unpaidUsers[] = $unpaidGroup->getPayer();
            }
        }





        return $this->render('document/payment.uncreated.html.twig', array("payers" => $unpaidUsers, "type" =>"invoice"));
    }

    /**
     * @Route("/document/invoice/jumper/{payer_id}")
     */
    public function getUnpaidByPayer($payer_id)
    {
        $currentUser = $this->forceLogin();
        $payer = $this->getDoctrine()->getRepository("AppBundle:User")->findOneById($payer_id);
        $unpaidJumpGroups = $this->jumpGroupRepository->findUnpaidByPayer($payer);
        $prepaidGroupRepository = $this->getDoctrine()->getRepository("AppBundle:PrepaidGroup");
        $unpaidPrepaidGroups = $prepaidGroupRepository->findUnpaidByPayer($payer);
        $voucherRepository = $this->getDoctrine()->getRepository("AppBundle:Voucher");


        return $this->render('document/payment.detail.html.twig', array(
            "unpaidJumpGroups" => $unpaidJumpGroups,
            "unpaidPrepaidGroups" =>$unpaidPrepaidGroups,

            "payer" => $payer


        ));
    }

    /**
     * @Route("/document/payment/create/")
     */
    public function createInvoice(Request $request)
    {
        $currentUser = $this->forceLogin();
        $post = EntityUtil::getArrayFromJson(RequestUtil::getPostAsJson($request->getContent()));
        $em = $this->getDoctrine()->getManager();
        $pdfGenerator = $this->get('spraed.pdf.generator');

        $payer = $this->getDoctrine()->getRepository("AppBundle:User")->findOneById($post['payer']);
        $unpaidJumpGroups = $this->jumpGroupRepository->findUnpaidByPayer($payer);

        $prepaidGroupRepository = $this->getDoctrine()->getRepository("AppBundle:PrepaidGroup");
        $unpaidPrepaidGroups = $prepaidGroupRepository->findUnpaidByPayer($payer);

        $voucherRepository = $this->getDoctrine()->getRepository("AppBundle:Voucher");


        $sum = 0;
        foreach ($unpaidJumpGroups as $unpaidJumpGroup) {
            $sum += $unpaidJumpGroup->getJumpType()->getPrice();
            if($payer->getIsMember()){
                $sum -= $unpaidJumpGroup->getJumpType()->getMemberDiscount();
            }
            if($unpaidJumpGroup->getVoucher() != null){
                $sum -= $unpaidJumpGroup->getVoucher()->getValue();
            }
            if($unpaidJumpGroup->getRent() != null && $unpaidJumpGroup->getRent()->getUser() == $payer &&   $unpaidJumpGroup->getRent()->getRenttype()->getPrice() > 0){
                $sum += $unpaidJumpGroup->getRent()->getRentType()->getPrice();
            }
        }
        foreach ($unpaidPrepaidGroups as $unpaidPrepaidGroup) {
            $sum += $unpaidPrepaidGroup->getPrepaidTypeGroup()->getPrice();
        }




        $invoice = new Invoice();
        $invoice->setInvoiceType("invoice");
        $invoice->setPaymentType($post['payment_type']);
        $invoice->setCreated(new \DateTime());
        $invoice->setSum($sum);
        $invoice->setInvoiceNumber(0);
        $invoice->setPayer($payer);
        $invoice->setUst(true);
        $em->persist($invoice);
        $em->flush();
        $invoiceNumber = "R-" . ((new \DateTime())->format('Y')) . "-" . $invoice->getId();
        $invoice->setInvoiceNumber($invoiceNumber);

        $html = $this->renderView("document/pdf/invoice.html.twig", array(
            "unpaidJumpGroups" => $unpaidJumpGroups,
            "unpaidPrepaidGroups" => $unpaidPrepaidGroups,
            "payer" => $payer,
            "invoice_number" => $invoiceNumber
        ));

        $pdf = $pdfGenerator->generatePDF($html);

        $invoice->setPdf($pdf);
        $em->merge($invoice);
        foreach ($unpaidJumpGroups as $unpaidJumpGroup) {
            $unpaidJumpGroup->setInvoice($invoice);
            $em->merge($unpaidJumpGroup);
        }
        foreach ($unpaidPrepaidGroups as $unpaidPrepaidGroup) {
            $unpaidPrepaidGroup->setInvoice($invoice);
            $em->merge($unpaidPrepaidGroup);
        }

        $em->flush();
        return $this->redirect("/document/invoice/overview");

    }

    /**
     * @Route("/document/credit/unpaid")
     */
    public function uncredit()
    {
        $currentUser = $this->forceLogin();
        $uncredits = $this->seatRepository->findAllUncredits();
        $uncreditUsers = array();
        foreach ($uncredits as $uncredit) {
            if (!in_array($uncredit->getJumper(), $uncreditUsers)) {
                $uncreditUsers[] = $uncredit->getJumper();
            }
        }

        return $this->render('document/payment.uncreated.html.twig', array("payers" => $uncreditUsers, "type" =>"credit"));
    }

    /**
     * @Route("/document/credit/jumper/{creditor_id}")
     */
    public function getCreditByCreditor($creditor_id)
    {
        $currentUser = $this->forceLogin();
        $creditor = $this->getDoctrine()->getRepository("AppBundle:User")->findOneById($creditor_id);
        $uncreditSeats = $this->seatRepository->findUncreditByCreditor($creditor);
        return $this->render('document/credit.detail.html.twig', array(
            "uncreditSeats" => $uncreditSeats,
            "creditor" => $creditor
        ));
    }


    /**
     * @Route("/document/credit/create/")
     */
    public function createCredit(Request $request)
    {
        $currentUser = $this->forceLogin();
        $post = EntityUtil::getArrayFromJson(RequestUtil::getPostAsJson($request->getContent()));
        $em = $this->getDoctrine()->getManager();
        $pdfGenerator = $this->get('spraed.pdf.generator');

        $creditor = $this->getDoctrine()->getRepository("AppBundle:User")->findOneById($post['creditor']);
        $creditSeats = $this->seatRepository->findUncreditByCreditor($creditor);
        $sum = 0.0;

        $creditSeatsToInvoice = array();
        foreach ($creditSeats as $key => $creditSeat) {
            if(!isset($post['c_'.$key])) continue;
            $creditSeatsToInvoice[] = $creditSeat;
        }
          foreach($creditSeatsToInvoice as $creditSeat){
            $sum += $creditSeat->getSalaryType()->getCredit();
              if($creditSeat->getJumpGroup()->getRent() != null && $creditSeat->getJumpGroup()->getRent()->getUser() == $creditor && $creditSeat->getJumpGroup()->getRent()->getRenttype()->getPrice() < 0){
                  $sum -= $creditSeat->getJumpGroup()->getRent()->getRentType()->getPrice();
              }
        }

        $credit = new Invoice();
        $credit->setPaymentType($post['payment_type']);
        $credit->setInvoiceType("credit");
        $credit->setCreated(new \DateTime());
        if(isset($post['ust'])){
            $sum += $sum * DocumentController::UST;
            $credit->setUst(true);
        }
        $credit->setSum($sum);
        $credit->setPayer($creditor);
        $credit->setInvoiceNumber(0);
        $em->persist($credit);
        $em->flush();
        $creditNumber = "G-" . ((new \DateTime())->format('Y')) . "-" . $credit->getId();

        $html = $this->renderView("document/pdf/credit.html.twig", array(
            "creditSeats" => $creditSeatsToInvoice,
            "creditor" => $creditor,
            "credit_number" => $creditNumber,
            "ust" => isset($post['ust'])
        ));


        $pdf = $pdfGenerator->generatePDF($html);

        $credit->setPdf($pdf);
        $em->merge($credit);


        foreach ($creditSeatsToInvoice as $creditSeat) {
            $creditSeat->setCredit($credit);
            $em->merge($creditSeat);
        }
        $em->flush();
        return $this->redirect("/document/credit/overview");

    }

    /**
     * @Route("/document/invoice/clear/{invoice_id}")
     */
    public function clearInvoice($invoice_id)
    {
        $currentUser = $this->forceLogin();
        $em = $this->getDoctrine()->getManager();
        $uncleared = $this->getDoctrine()->getRepository("AppBundle:Invoice")->findOneById($invoice_id);
        $uncleared->setCleared(true);
        $em->merge($uncleared);
        if($uncleared->getPaymentType() == "cash"){
            $debit = new Debit();
            $debit->setLoginUser($currentUser);
            $debit->setCreated(new \DateTime());
            if($uncleared->getInvoiceType() == "invoice"){
                $debit->setSum($uncleared->getSum());
            }
            if($uncleared->getInvoiceType() == "credit"){
                $debit->setSum("-1" * $uncleared->getSum());
            }
            $em->persist($debit);
        }
        $em->flush();
        return $this->redirect("/document/".$uncleared->getInvoiceType()."/overview");
    }


    /**
     * @Route("/document/{type}/overview/{filter}/{page}")
     */
    public function invoiceOverview($type, $filter = 0, $page = 1)
    {
        $currentUser = $this->forceLogin();
        $creditRepository = $this->getDoctrine()->getRepository("AppBundle:Invoice");
        $pagination = $creditRepository->getAllPaged($filter, $type);
        $pages = $pagination->getPagesCount();

        return new Response($this->renderView("document/invoice.overview.html.twig", array("positions" => $pagination->getPage($page),
                "pages" => $pages,
                "page" => $page,
                "type" => $type,
                "filter" => $filter
            )
        ));


    }

    /**
     * @Route("/document/invoice/storno/{invoice_id}")
     */
    public function storno($invoice_id)
    {
        $currentUser = $this->forceLogin();
        $invoiceRepository = $this->getDoctrine()->getRepository("AppBundle:Invoice");
        $em = $this->getDoctrine()->getManager();
        $invoice = $invoiceRepository->findOneById($invoice_id);
        if($invoice->getCleared()){
            throw new BadProcessException();
        }
        if ($invoice->getInvoiceType() == "invoice") {
            $jumpGroups = $invoiceRepository->findJumpGroupsByInvoice($invoice);
            foreach ($jumpGroups as $jumpGroup) {
                $jumpGroup->setInvoice(null);
                $em->merge($jumpGroup);
            }
            $prepaidGroupRepository = $this->getDoctrine()->getRepository("AppBundle:PrepaidGroup");
            $prepaidGroups = $prepaidGroupRepository->findByInvoice($invoice);
            foreach ($prepaidGroups as $prepaidGroup) {
                $prepaidGroup->setInvoice(null);
                $em->merge($prepaidGroup);
            }

        } else {
            $seats = $invoiceRepository->findSeastByCredit($invoice);

            foreach ($seats as $seat) {
                $seat->setCredit(null);
                $em->merge($seat);
            }
        }

        $em->remove($invoice);
        $em->flush();
        return $this->redirect("/document/".$invoice->getInvoiceType()."/overview");

    }


}
