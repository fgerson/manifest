<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Qualification;
use AppBundle\Entity\Rent;
use AppBundle\Entity\Voucher;
use AppBundle\Service\SessionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Util\RequestUtil;
use AppBundle\Util\EntityUtil;
use AppBundle\Entity\User;
use AppBundle\Entity\Load;
use AppBundle\Entity\JumpGroup;
use AppBundle\Entity\Seat;
use AppBundle\Controller\ParentController;
use AppBundle\Exception\BadProcessException;

class LoadController extends ParentController
{


    private $loadRepository;
    private $userRepository;
    private $jumptypeRepository;
    private $prepaidService;

    public function __construct(


        \AppBundle\Repository\LoadRepository $loadRepository,
        \AppBundle\Repository\UserRepository $userRepository,
        \AppBundle\Repository\JumptypeRepository $jumptypeRepository,
        \AppBundle\Service\PrepaidService $prepaidService


    )
    {

        $this->loadRepository = $loadRepository;
        $this->userRepository = $userRepository;
        $this->jumptypeRepository = $jumptypeRepository;
        $this->prepaidService = $prepaidService;

    }

    /**
     * @Route("/load/edit/{load_id}")
     */
    public function edit($load_id)
    {
        $currentUser = $this->forceLogin();

        $qualificationRepository = $this->getDoctrine()->getRepository("AppBundle:Qualification");
        $qualificationEntities = $qualificationRepository->findAll();
        $qualifications = array();
        foreach ($qualificationEntities as $qualificationEntity) {
            $qualifications[$qualificationEntity->getName()] = $this->userRepository->findByQualification($qualificationEntity->getName());
        }
        return new Response($this->renderLoadView('load/edit.html.twig', array(
                "load" => $this->loadRepository->findOneById($load_id),
                "users" => $this->userRepository->findAll(),
                "jumpers" => $this->userRepository->findAllAvailableNotGuests(),
                "qualifications" => $qualifications,
                "jumptypes" => $this->getDoctrine()->getRepository("AppBundle:Jumptype")->findAll(),
                "salaryTypes" => $this->getDoctrine()->getRepository("AppBundle:SalaryType")->findAll(),
                "rentTypes" => $this->getDoctrine()->getRepository("AppBundle:RentType")->findAll())
        ));
    }

    /**
     * @Route("/load/create")
     */
    public function create(Request $request)
    {
        $currentUser = $this->forceLogin();
        $load = new Load();
        $load->setCreated(new \DateTime());
        $em = $this->getDoctrine()->getManager();
        $em->persist($load);
        $em->flush();

        return $this->redirect("/load/overview");

    }

    /**
     * @Route("/load/addseat/solo/{load_id}")
     */
    public function addSoloSeat(Request $request, $load_id)
    {
        $currentUser = $this->forceLogin();
        $json = RequestUtil::getPostAsJson($request->getContent());
        $post = EntityUtil::getArrayFromJson($json);

        $em = $this->getDoctrine()->getEntityManager();
        $jumpGroup = new JumpGroup();

        $load = $this->loadRepository->findOneById($load_id);
        if ($load->getStarted()) throw new BadProcessException();
        $jumpGroup->setLoad($load);
        $jumpGroup->setJumpType($this->getDoctrine()->getRepository("AppBundle:Jumptype")->findOneById($post['jumptype']));


        $payer = $this->userRepository->findOneById($post['payed_by']);
        $jumpGroup->setPayer($payer);
        $em->persist($jumpGroup);

        if ($post['rent'] != 0) {
            $rentType = $this->getDoctrine()->getRepository("AppBundle:RentType")->findOneById($post['rent']);
            $rent = new Rent();
            $rent->setRentType($rentType);
            $rent->setUser($payer);
            $rent->setJumpGroup($jumpGroup);
            $em->persist($rent);
        }
        if (isset($post['use_block'])) {
            $jumptype = $this->jumptypeRepository->findOneById($post['jumptype']);
            $jumpGroup->setPrepaid($this->prepaidService->getAvailablePrepaid($payer, $jumptype));
            $em->merge($jumpGroup);

        }


        $seat = new Seat();
        $seat->setJumpGroup($jumpGroup);


        $jumper = $this->userRepository->findOneById($post['jumped_by']);
        $seat->setJumper($jumper);
        $em->persist($seat);


        $em->flush();

        return $this->redirect("/load/edit/" . $load_id);
    }

    /**
     * @Route("/load/addseat/tandem/{load_id}")
     */
    public function addTandemSeat(Request $request, $load_id)
    {
        $currentUser = $this->forceLogin();
        $json = RequestUtil::getPostAsJson($request->getContent());
        $post = EntityUtil::getArrayFromJson($json);

        $em = $this->getDoctrine()->getEntityManager();
        $load = $this->loadRepository->findOneById($load_id);
        if ($load->getStarted()) throw new BadProcessException();
        $jumpGroup = new JumpGroup();
        $jumpGroup->setLoad($load);
        $jumpGroup->setJumpType($this->getDoctrine()->getRepository("AppBundle:Jumptype")->findOneById($post['jumptype']));
        $jumper = $this->userRepository->findOneById($post['jumped_by']);

        $payer = $this->userRepository->findOneById($post['payed_by']);

        $jumpGroup->setPayer($payer);

        if (!empty($post['voucher'])) {

            $voucher = new Voucher();
            $voucher->setNumber($post['voucher']);
            $voucher->setValue($post['voucher_value']);
            $em->persist($voucher);
            $jumpGroup->setVoucher($voucher);

        }
        $em->persist($jumpGroup);

        $passengerSeat = new Seat();
        $passengerSeat->setJumpGroup($jumpGroup);
        $passengerSeat->setJumper($jumper);
        $em->persist($passengerSeat);

        $tandemmasterSeat = new Seat();
        $tandemmasterSeat->setJumpGroup($jumpGroup);
        $tandemmaster = $this->userRepository->findOneById($post['tandem_master']);
        $tandemmasterSeat->setJumper($tandemmaster);
        if ($post['seat_type_tm'] != 0) {
            $tmSalaryType = $this->getDoctrine()->getRepository("AppBundle:SalaryType")->findOneById($post['seat_type_tm']);
            $tandemmasterSeat->setSalaryType($tmSalaryType);
        }

        $em->persist($tandemmasterSeat);
        if ($post['rent'] != 0) {
            $rentType = $this->getDoctrine()->getRepository("AppBundle:RentType")->findOneById($post['rent']);
            $rent = new Rent();
            $rent->setRentType($rentType);
            $rent->setUser($tandemmaster);
            $rent->setJumpGroup($jumpGroup);
            $em->persist($rent);
        }
        if (!empty($post['video'])) {
            $videoSeat = new Seat();
            $videoSeat->setJumpGroup($jumpGroup);
            $video = $this->userRepository->findOneById($post['video']);
            $videoSeat->setJumper($video);
            $videoSalaryType = $this->getDoctrine()->getRepository("AppBundle:SalaryType")->findOneById($post['seat_type_video']);
            $videoSeat->setSalaryType($videoSalaryType);
            $em->persist($videoSeat);
        }


        $em->flush();

        return $this->redirect("/load/edit/" . $load_id);
    }

    /**
     * @Route("/load/addseat/aff/{load_id}")
     */
    public function addAFFseat(Request $request, $load_id)
    {
        $currentUser = $this->forceLogin();
        $json = RequestUtil::getPostAsJson($request->getContent());
        $post = EntityUtil::getArrayFromJson($json);

        $em = $this->getDoctrine()->getEntityManager();
        $load = $this->loadRepository->findOneById($load_id);
        if ($load->getStarted()) throw new BadProcessException();
        $jumpGroup = new JumpGroup();
        $jumpGroup->setLoad($load);
        $jumpGroup->setJumpType($this->getDoctrine()->getRepository("AppBundle:Jumptype")->findOneById($post['jumptype']));
        $jumper = $this->userRepository->findOneById($post['jumped_by']);

        $payer = $this->userRepository->findOneById($post['payed_by']);

        $jumpGroup->setPayer($payer);
        $em->persist($jumpGroup);

        $studentSeat = new Seat();
        $studentSeat->setJumpGroup($jumpGroup);
        $studentSeat->setJumper($jumper);
        $em->persist($studentSeat);

        $aff1Seat = new Seat();
        $aff1Seat->setJumpGroup($jumpGroup);
        $aff1 = $this->userRepository->findOneById($post['aff_1']);
        $aff1Seat->setJumper($aff1);
        $aff1SalaryType = $this->getDoctrine()->getRepository("AppBundle:SalaryType")->findOneById($post['salary_type_aff_1']);
        $aff1Seat->setSalaryType($aff1SalaryType);
        $em->persist($aff1Seat);
        if (!empty($post['aff_2'])) {
            $aff2Seat = new Seat();
            $aff2Seat->setJumpGroup($jumpGroup);
            $aff2 = $this->userRepository->findOneById($post['aff_2']);
            $aff2Seat->setJumper($aff2);
            $aff1SalaryType = $this->getDoctrine()->getRepository("AppBundle:SalaryType")->findOneById($post['salary_type_aff_2']);
            $aff2Seat->setSalaryType($aff1SalaryType);
            $em->persist($aff2Seat);

        }
        if (!empty($post['video'])) {
            $videoSeat = new Seat();
            $videoSeat->setJumpGroup($jumpGroup);
            $video = $this->userRepository->findOneById($post['video']);
            $videoSeat->setJumper($video);
            $videoSalaryType = $this->getDoctrine()->getRepository("AppBundle:SalaryType")->findOneById($post['seat_type_video']);
            $videoSeat->setSalaryType($videoSalaryType);
            $em->persist($videoSeat);
        }


        $em->flush();

        return $this->redirect("/load/edit/" . $load_id);
    }

    /**
     * @Route("/load/addseat/instructor/{load_id}")
     */
    public function addInstructorSeat(Request $request, $load_id)
    {
        $currentUser = $this->forceLogin();
        $json = RequestUtil::getPostAsJson($request->getContent());
        $post = EntityUtil::getArrayFromJson($json);

        $em = $this->getDoctrine()->getEntityManager();
        $load = $this->loadRepository->findOneById($load_id);
        if ($load->getStarted()) throw new BadProcessException();
        $jumpGroup = new JumpGroup();
        $jumpGroup->setLoad($load);
        $jumpGroup->setJumpType($this->getDoctrine()->getRepository("AppBundle:Jumptype")->findOneById($post['jumptype']));
        $jumper = $this->userRepository->findOneById($post['jumped_by']);


        $jumpGroup->setPayer($jumper);
        $em->persist($jumpGroup);

        $studentSeat = new Seat();
        $studentSeat->setJumpGroup($jumpGroup);
        $studentSeat->setJumper($jumper);
        $em->persist($studentSeat);

        $instructorSeat = new Seat();
        $instructorSeat->setJumpGroup($jumpGroup);
        $instructor = $this->userRepository->findOneById($post['instructor']);
        $instructorSeat->setJumper($instructor);
        $instrctorSalaryType = $this->getDoctrine()->getRepository("AppBundle:SalaryType")->findOneById($post['salary_type_instructor']);
        $instructorSeat->setSalaryType($instrctorSalaryType);
        $em->persist($instructorSeat);


        $em->flush();

        return $this->redirect("/load/edit/" . $load_id);
    }


    /**
     * @Route("/load/overview/{date}")
     */
    public function overview($date = null)
    {
        $currentUser = $this->forceLogin();
        if ($date == null) {
            $date = new \DateTime();
        } else {
            $date = \DateTime::createFromFormat("d.m.Y", base64_decode($date));
        }

        $loads = $this->loadRepository->getLoadsByDate($date);
        return new Response($this->renderLoadView('load/overview.html.twig', array("loads" => $loads, "date" => $date->format("d.m.Y"))));
    }

    /**
     * @Route("/load/removejumpgroup/{load_id}/{jg_id}")
     */
    public function removeJumpgroup($load_id, $jg_id)
    {
        $currentUser = $this->forceLogin();
        $em = $this->getDoctrine()->getManager();
        $jumpGroupRepository = $this->getDoctrine()->getRepository("AppBundle:JumpGroup");
        $jumpGroup = $jumpGroupRepository->findOneById($jg_id);
        if ($jumpGroup->getLoad()->getStarted() != null) throw new BadProcessException();
        if ($jumpGroup->getInvoice() != null) {
            throw new BadProcessException();
        }
        foreach ($jumpGroup->getSeats() as $seat) {
            if ($seat->getCredit() != null) throw new BadProcessException();
        }
        foreach ($jumpGroup->getSeats() as $seat) {
            $em->remove($seat);
        }
        if ($jumpGroup->getRent() != null) {
            $em->remove($jumpGroup->getRent());
        }
        if($jumpGroup->getVoucher() != null){
            $em->remove($jumpGroup->getVoucher());
        }
        $em->remove($jumpGroup);
        $em->flush();
        return $this->redirect("/load/edit/" . $load_id);
    }

    /**
     * @Route("/load/start/{load_id}/{time}")
     */
    public function start($load_id, $time)
    {
        $currentUser = $this->forceLogin();
        $time = base64_decode($time);

        $em = $this->getDoctrine()->getManager();
        $load = $this->loadRepository->findOneById($load_id);
        $startDateTime = new \DateTime();
        $time = explode(":", $time);
        $startDateTime->setTime($time[0], $time[1]);
        $load->setStarted($startDateTime);
        $em->merge($load);
        $em->flush();
        $this->recalculateStartTimes();
        return $this->redirect("/load/overview");
    }

    /**
     * @Route("/load/schedule/{load_id}/connected/{connected}")
     * @Route("/load/schedule/{load_id}/{time}")
     */
    public function schedule($load_id, $time = null, $connected = false)
    {
        $currentUser = $this->forceLogin();
        $em = $this->getDoctrine()->getManager();
        $load = $this->loadRepository->findOneById($load_id);
        if ($time == null) {
            $load->setShouldFollow($connected);
            $em->merge($load);

        } else {
            $time = base64_decode($time);
            $startDateTime = new \DateTime();

            $time = explode(":", $time);
            $startDateTime->setTime($time[0], $time[1]);
            $load->setShouldFollow(false);
            $load->setScheduledStart($startDateTime);
            $em->merge($load);
        }
        $this->recalculateStartTimes();

        //recalculate


        $em->flush();
        return $this->redirect("/load/overview");
    }

    /**
     * @Route("/load/unstart/{load_id}")
     */
    public
    function unstart($load_id)
    {
        $currentUser = $this->forceLogin();
        $em = $this->getDoctrine()->getManager();
        $load = $this->loadRepository->findOneById($load_id);
        $load->setStarted(null);
        $em->merge($load);
        $em->flush();
        $this->recalculateStartTimes();
        return $this->redirect("/load/overview");
    }

    /**
     * @Route("/load/delete/{load_id}")
     */
    public
    function delete($load_id)
    {
        $currentUser = $this->forceLogin();
        $em = $this->getDoctrine()->getManager();
        $load = $this->loadRepository->findOneById($load_id);
        foreach ($load->getJumpGroups() as $jumpGroup) {

            if($jumpGroup->getVoucher() != null){
                $em->remove($jumpGroup->getVoucher());
            }

            foreach ($jumpGroup->getSeats() as $seat) {

                $em->remove($seat);
            }
            if ($jumpGroup->getRent() != null) {
                $em->remove($jumpGroup->getRent());
            }
            $em->remove($jumpGroup);
        }
        $em->remove($load);


        $em->flush();
        $this->recalculateStartTimes();
        return $this->redirect("/load/overview");
    }

    private function recalculateStartTimes()
    {
        $em = $this->getDoctrine()->getManager();
        $loadsToday = $this->loadRepository->getLoadsByDate(new \DateTime());
        $lastStartTime = null;
        foreach ($loadsToday as $load) {
            if ($lastStartTime != null && $load->getShouldFollow()) {
                $newStartTime = clone $lastStartTime;
                $load->setScheduledStart($newStartTime->add(new \DateInterval('PT20M')));
                $em->merge($load);
            }
            $lastStartTime = $load->getStarted() != null ? $load->getStarted() : ($load->getScheduledStart() != null ? $load->getScheduledStart() : null);

        }
        $em->flush();
    }
}