<?php

namespace AppBundle\Controller;

use AppBundle\Dto\PrepaidDto;
use AppBundle\Dto\PrepaidGroupDto;
use AppBundle\Entity\Debit;
use AppBundle\Entity\Prepaid;
use AppBundle\Entity\PrepaidGroup;
use AppBundle\Repository\PrepaidRepository;
use AppBundle\Service\PrepaidService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use AppBundle\Util\RequestUtil;
use AppBundle\Util\EntityUtil;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Exception\BadProcessException;

class BlockController extends ParentController
{


    private $prepaidService;
    private $prepaidRepository;

    public function __construct(


        PrepaidService $prepaidService,
        PrepaidRepository $prepaidRepository


    )
    {


        $this->prepaidService = $prepaidService;
        $this->prepaidRepository = $prepaidRepository;

    }


    /**
     * @Route("/block/buy")
     */
    public function buy(Request $request)
    {
        $currentUser = $this->forceLogin();
        $prepaidTypeGroupRepository = $this->getDoctrine()->getRepository("AppBundle:PrepaidTypeGroup");
        $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");
        if ($request->getContent() == null) {
            $availableJumpers = $userRepository->findAllNotGuests();
            $blockGroups = $prepaidTypeGroupRepository->findAll();

            return new Response($this->renderView("block/buy.html.twig", array(
                "jumpers" => $availableJumpers,
                "blockGroups" => $blockGroups
            )));
        } else {

            $em = $this->getDoctrine()->getManager();

            $post = EntityUtil::getArrayFromJson(RequestUtil::getPostAsJson($request->getContent()));

            $user = $userRepository->findOneById($post['jumper_id']);
            $prepaidTypeGroup = $prepaidTypeGroupRepository->findOneById($post['block']);
            $prepaidTypes = $prepaidTypeGroup->getPrepaidTypes();

            $prepaidGroup = new PrepaidGroup();
            $prepaidGroup->setPrepaidTypeGroup($prepaidTypeGroup);
            $prepaidGroup->setPayer($user);
            $prepaidGroup->setBuyedAt(new \DateTime());
            $em->persist($prepaidGroup);

            foreach ($prepaidTypes as $prepaidType) {
                $prepaid = new Prepaid();
                $prepaid->setUser($user);
                $prepaid->setPrepaidType($prepaidType);
                $prepaid->setPrepaidGroup($prepaidGroup);
                $em->persist($prepaid);
            }


            $em->flush();
            return $this->redirect("/block/overview");
        }

    }

    /**
     * @Route("/block/overview/{user_id}/{page}")
     * @Route("/block/overview/{page}")
     */
    public function overview($user_id = null, $page = 1)
    {
        $currentUser = $this->forceLogin();
        $prepaidGroupRepository = $this->getDoctrine()->getRepository("AppBundle:PrepaidGroup");
        $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");
        $prepaidRepository = $this->getDoctrine()->getRepository("AppBundle:Prepaid");
        if ($user_id != null) {
            $user = $userRepository->findOneById($user_id);
            $pagination = $prepaidGroupRepository->findByUserPaged($user);
        } else {
            $user = null;
            $pagination = $prepaidGroupRepository->findAllPaged();
        }


        $pages = $pagination->getPagesCount();
        $prepaidGroups = $pagination->getPage($page);

        $prepaidGroupDtos = array();
        foreach ($prepaidGroups as $prepaidGroup) {
            $prepaidGroupDto = new PrepaidGroupDto();
            $prepaidGroupDto->setPrepaidGroup($prepaidGroup);
            $deleteable = $prepaidGroup->getInvoice() == null;
            foreach ($prepaidGroup->getPrepaids() as $prepaid) {
                $prepaidDto = new PrepaidDto();
                $prepaidDto->setPrepaid($prepaid);
                $prepaidDto->setCapacity($this->prepaidService->getCapacityForPrepaid($prepaid));
                $prepaidGroupDto->addPrepaidDto($prepaidDto);
                $deleteable = $deleteable ?  $prepaidDto->getCapacity() == $prepaid->getPrepaidType()->getQuantity() : false;
            }


            $prepaidGroupDto->setDeleteable($deleteable);

            $prepaidGroupDtos[] = $prepaidGroupDto;
        }

        return new Response($this->renderView("block/overview.html.twig", array(
            "page" => $page,
            "pages" => $pages,
            "prepaidGroupDtos" => $prepaidGroupDtos,
            "jumpers" => $userRepository->findAllNotGuests(),
            "jumper" => $user
        )));


    }

    /**
     * @Route("/block/delete/{prepaid_group_id}")
     */
    public function delete($prepaid_group_id)
    {
        $currentUser = $this->forceLogin();
        $em = $this->getDoctrine()->getManager();
        $prepaidGroupRepository = $this->getDoctrine()->getRepository("AppBundle:PrepaidGroup");
        $prepaidGroup = $prepaidGroupRepository->findOneById($prepaid_group_id);

        if ($prepaidGroup->getInvoice() != null) {
            throw new BadProcessException();
        }
        foreach ($prepaidGroup->getPrepaids() as $prepaid) {
            $remainingCapacity = $this->prepaidService->getCapacityForPrepaid($prepaid);
            if ($prepaid->getPrepaidType()->getQuantity() != $remainingCapacity) {
                throw new BadProcessException();
            }
            $em->remove($prepaid);
        }


       $em->remove($prepaidGroup);
        $em->flush();
        return $this->redirect("/block/overview");


    }

}
