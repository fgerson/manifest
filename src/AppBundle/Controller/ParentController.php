<?php
namespace AppBundle\Controller;
use AppBundle\Entity\Session;
use AppBundle\Service\PrepaidService;
use AppBundle\Service\SessionService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
abstract class ParentController extends Controller
{

    private $sessionService;

    /**
     * @param SessionService $sessionService
     * @return void
     * @required
     */
    public function injectSessionService(SessionService $sessionService){
        $this->sessionService = $sessionService;
    }

    public function getSessionService(){
        return $this->sessionService;
    }

    public function renderLoadView($template, $data = array()){
        $loadRepository = $this->getDoctrine()->getRepository("AppBundle:Load");
        $loadsOfToday = $loadRepository->getLoadsByDate(new \DateTime());
        $data['loads_of_today'] = $loadsOfToday;

        return $this->renderView($template, $data);
    }



    protected function forceLogin(){

        if($this->sessionService->getCurrentUser() == null){
            header("Location: /login");
            die();
        }else{
            return $this->getCurrentUser();
        }
    }

    protected function getCurrentUser(){

        return $this->sessionService->getCurrentUser();
    }
}
