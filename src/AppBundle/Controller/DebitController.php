<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Debit;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use AppBundle\Util\RequestUtil;
use AppBundle\Util\EntityUtil;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;

class DebitController extends ParentController
{




    private $debitService;
    public function __construct(
        \AppBundle\Service\DebitService $debitService

    ){
        $this->debitService = $debitService;

    }

    /**
     * @Route("/cash/change/{type}")
     */
    public function take(Request $request, $type)
    {
        $currentUser = $this->forceLogin();
        if($request->getContent() == null){
            return new Response($this->renderView("debit/change.html.twig", array("type" => $type)));
        }else{
            $post = EntityUtil::getArrayFromJson(RequestUtil::getPostAsJson($request->getContent()));
            $em = $this->getDoctrine()->getManager();
            $debit = new Debit();
            $debit->setLoginUser($currentUser);
            $post['value'] = str_replace(",", ".", $post['value']);


            $sum = $post['value'] * ($type == "take" ? -1 :1);

            $debit->setSum($sum);
            $debit->setCreated(new \DateTime());
            $em->persist($debit);
            $em->flush();
            return $this->redirect("/cash/overview");
        }

    }

    /**
     * @Route("/cash/overview/{page}")
     */
    public function overview($page = 1)
    {
        $currentUser = $this->forceLogin();
        $repository = $this->getDoctrine()->getRepository("AppBundle:Debit");
        $pagination = $repository->getAllPaged();

        return new Response($this->renderView("debit/overview.html.twig", array(
            "page" =>$page,
            "pages" =>$pagination->getPagesCount(),
            "debits" =>$pagination->getPage($page),
            "balance" => $this->debitService->getBalance()
        )));

    }
}
