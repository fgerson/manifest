<?php

namespace AppBundle\Controller;

use AppBundle\Util\TimeUtil;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Util\RequestUtil;
use AppBundle\Util\EntityUtil;
use AppBundle\Entity\User;

class UserController extends ParentController
{

    private $userRepository;
    private $userService;

    public function __construct(\AppBundle\Repository\UserRepository $userRepository,
                                \AppBundle\Service\UserService $userService
    )
    {
        $this->userRepository = $userRepository;
        $this->userService = $userService;
    }

    /**
     * @Route("/user/overview//{filter}/{page}")
     * @Route("/user/overview/{search}/{filter}/{page}")
     */
    public function search(Request $request, $search = null, $filter = 0, $page = 1)
    {
        $currentUser = $this->forceLogin();
        if ($search == null) {
            $pagination = $this->userRepository->find(null, $filter);
        } else {
            $pagination = $this->userRepository->find($search, $filter);
        }

        return $this->render("user/overview.html.twig", array(
            "page" => $page,
            "pages" => $pagination->getPagesCount(),
            "users" => $pagination->getPage($page),
            "search" => $search,
            "filter" => $filter,
         "firstDayOfYear" => TimeUtil::getFirstDayOfYear()
        ));
    }

    /**
     * @Route("/user/create")
     */
    public function create(Request $request)
    {
        $currentUser = $this->forceLogin();
        $qualificationRepository = $this->getDoctrine()->getRepository("AppBundle:Qualification");
        $qualifications = $qualificationRepository->findAll();
        if ($request->getContent() == null) {
            return $this->render('user/create.html.twig', array("qualifications" => $qualifications));
        }
        $json = RequestUtil::getPostAsJson($request->getContent());
        $post = EntityUtil::getArrayFromJson($json);
        $user = EntityUtil::getObjectFromJson($json, "AppBundle\Entity\User");
        foreach (EntityUtil::getArrayFromJson($json)['qualification_tags'] as $qualification_tag) {
            $qualification = $qualificationRepository->findOneByName($qualification_tag);
            if ($qualification != null) {
                $user->addQualification($qualification);
            }

        }
        if (!empty($post['reserve'])) {
            $user->setReserve(\DateTime::createFromFormat("d.m.Y", $post['reserve']));
        }

        if (!empty($post['disclaimer_date'])) {
            $user->setDisclaimerDate(\DateTime::createFromFormat("d.m.Y", $post['disclaimer_date']));
        }

        if (!empty($post['license_exp'])) {
            $user->setLicenseExpiration(\DateTime::createFromFormat("d.m.Y", $post['license_exp']));
        }
        $user->setIsGuest(isset($post['guest']));
        $error = array();
        if (!$this->userService->checkIBAN($user)) {
            $error[] = "IBAN ist ungültig";
        }
        if (count($error) == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->redirect("/user/overview");
        } else {

            return $this->showUserEdit($user, EntityUtil::getArrayFromJson($json)['qualification_tags'], $error);
        }


        //return $this->redirect("/user/overview");

    }

    /**
     * @Route("/user/edit/{userid}")
     */
    public function edit(Request $request, $userid)
    {
        $currentUser = $this->forceLogin();
        $existingUser = $this->userRepository->findOneById($userid);
        if ($request->getContent() == null) {
            return $this->showUserEdit($existingUser);
        }
        $json = RequestUtil::getPostAsJson($request->getContent());
        $user = EntityUtil::getObjectFromJson($json, "AppBundle\Entity\User");
        $post = EntityUtil::getArrayFromJson($json);

        $user->setIsGuest(isset($post['guest']));
        $user->setIsMember(isset($post['member']));

        $user->setReserve(null);
        $user->setLicenseExpiration(null);
        $user->setDisclaimerDate(null);


        if (!empty($post['reserve'])) {

            $user->setReserve(\DateTime::createFromFormat("d.m.Y", $post['reserve']));
        }

        if (!empty($post['license_exp'])) {

            $user->setLicenseExpiration(\DateTime::createFromFormat("d.m.Y", $post['license_exp']));
        }
        if (!empty($post['disclaimer_date'])) {

            $user->setDisclaimerDate(\DateTime::createFromFormat("d.m.Y", $post['disclaimer_date']));
        }

        $error = array();
        if (!$this->userService->checkIBAN($user)) {
            $error[] = "IBAN ist ungültig";
        }
        if (count($error) == 0) {
            $this->userService->updateUser($user, $userid, EntityUtil::getArrayFromJson($json)['qualification_tags']);
            return $this->redirect("/user/overview");
        } else {
            return $this->showUserEdit($user, EntityUtil::getArrayFromJson($json)['qualification_tags'], $error);
        }


    }

    private function showUserEdit(User $user, $qualifications = null, $errors = array())
    {
        $qualificationRepository = $this->getDoctrine()->getRepository("AppBundle:Qualification");

        $qualifications = $qualificationRepository->findAll();
        if ($qualifications != null) {
            foreach ($qualifications as $qualification) {
                $user->addQualification($qualification);
            }
        }

        return $this->render('user/create.html.twig', array("user" => $user, "qualifications" => $qualifications, "errors" => $errors, "firstDayOfYear" => TimeUtil::getFirstDayOfYear()));
    }
}
