<?php

namespace AppBundle\Controller;

use AppBundle\Util\EntityUtil;
use AppBundle\Util\HashUtil;
use AppBundle\Util\RequestUtil;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Controller\ParentController;

class LoginController extends ParentController
{


    /**
     * @Route("/login")
     */
    public function login(Request $request)
    {
        if ($request->getContent() == null) {
            return new Response($this->renderView("default/login.html.twig"));
        } else {
            $post = EntityUtil::getArrayFromJson(RequestUtil::getPostAsJson($request->getContent()));
           if( $this->getSessionService()->login($post['username'], $post['password'])){
              return $this->redirect("/");
           }else{
               return new Response($this->renderView("default/login.html.twig", array("error" => true)));
           }
        }
    }

    /**
     * @Route("/changePassword")
     */
    public function changePassword(Request $request)
    {
        $currentUser = $this->forceLogin();
        if ($request->getContent() == null) {
            return new Response($this->renderView("default/changePassword.html.twig"));
        } else {
            $post = EntityUtil::getArrayFromJson(RequestUtil::getPostAsJson($request->getContent()));
            if($currentUser->getPassword() == HashUtil::SHA256($post['password_old'])){
                $currentUser->setPassword(HashUtil::SHA256($post['password_new']));
                $em = $this->getDoctrine()->getManager();
                $em->merge($currentUser);
                $em->flush();
                return $this->redirect("/");
            }else{
                return new Response($this->renderView("default/changePassword.html.twig", array("error" => true)));
            }
        }
    }

    /**
     * @Route("/logout")
     */
    public function logout()
    {
       $this->getSessionService()->logout();
       return $this->redirect("/");
    }
}
