<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Invoice;
use AppBundle\Util\PaginationUtil;
use Doctrine\ORM\EntityRepository;

class DebitRepository extends EntityRepository
{
    public function getAllPaged()

    {

        $query = $this->getEntityManager()
            ->createQuery(
                "SELECT c from AppBundle:Debit c  ORDER BY c.id DESC ");
        $pagination = new PaginationUtil();
        $pagination->setQuery($query);
        return $pagination;
    }



}
