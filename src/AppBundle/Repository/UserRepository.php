<?php

namespace AppBundle\Repository;

use AppBundle\Util\PaginationUtil;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{


    public function findAllAvailableNotGuests(){

        $yearAgo = (new \DateTime())->modify("- 1 year");

        return $this->getEntityManager()
            ->createQuery(
                'SELECT u from AppBundle:User u where u.deleted = 0 and u.isGuest = 0 and u.reserve is not null and u.reserve >= :reserve and u.disclaimer_date is not null and u.disclaimer_date >= :disclaimer and u.license_expiration >= CURRENT_TIMESTAMP ()')
            ->setParameter(':reserve', $yearAgo)
            ->setParameter(':disclaimer', $yearAgo)

            ->getResult();
        ;


    }
   public function findByQualification($qualification){
      return $this->getEntityManager()
           ->createQuery(
               'SELECT u from AppBundle:User u inner join u.qualifications q where q.name = ?0 and u.deleted = 0')
          ->setParameters(array($qualification))
          ->getResult();
       ;

   }

    public function find($search = null, $filter = 0){
        $query = $this->getEntityManager()
            ->createQuery(
                'SELECT u from AppBundle:User u where u.deleted = 0 and (?0 is null OR (u.first_name like ?0 or u.last_name like ?0)) and (?1 = 0  OR  (?1 = 1 and u.isGuest = 0) OR  (?1 = 2 and u.isGuest = 1))')->setParameters(array("%".$search."%", $filter))
        ;
        $paginationUtil = new PaginationUtil();
        $paginationUtil->setQuery($query);
        return $paginationUtil;
    }
}