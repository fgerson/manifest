<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Jumptype;
use AppBundle\Entity\Prepaid;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Proxies\__CG__\AppBundle\Entity\PrepaidType;

class JumpGroupRepository extends EntityRepository
{

    public function findAllUnpaid(){

        return $this->getEntityManager()->createQuery(
            'SELECT jg FROM AppBundle:JumpGroup jg where jg.invoice is null AND jg.prepaid is null')
            ->getResult();
    }


    public function findUnpaidByPayer($payer){

        return $this->getEntityManager()->createQuery(
            'SELECT jg FROM AppBundle:JumpGroup jg where jg.invoice is null AND jg.prepaid is null  and jg.payer = :payer')
            ->setParameter(":payer", $payer)
            ->getResult();
    }
    public function findByUserAndJumpType(User $payer, Jumptype $jumptype){

        return $this->getEntityManager()->createQuery(
            'SELECT jg FROM AppBundle:JumpGroup jg inner join jg.prepaid p inner join p.prepaid_type pt where  jg.payer = :payer AND pt.jumpType = :jumpType')
            ->setParameter(":payer", $payer)
            ->setParameter(":jumpType", $jumptype)
            ->getResult();
    }


}