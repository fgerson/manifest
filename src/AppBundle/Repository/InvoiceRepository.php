<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Invoice;
use AppBundle\Util\PaginationUtil;
use Doctrine\ORM\EntityRepository;

class InvoiceRepository extends EntityRepository
{
    public function getAllPaged($filter = 0, $type = "invoice")

    {

        $query =  $this->getEntityManager()
            ->createQuery(
                "SELECT c from AppBundle:Invoice c where (?0 <> 1  OR c.cleared = 0) AND c.invoice_type = ?1 ORDER BY c.id DESC "
            )
            ->setParameters(array($filter, $type));
        $pagination = new PaginationUtil();
        $pagination->setQuery($query);
        return $pagination;
    }

    public function findJumpGroupsByInvoice(Invoice $invoice){
        return $this->getEntityManager()
            ->createQuery(
                "SELECT j from AppBundle:JumpGroup j where j.invoice = :invoice ")
            ->setParameter(":invoice", $invoice)
            ->getResult();
    }

    public function findSeastByCredit(Invoice $invoice){
        return $this->getEntityManager()
            ->createQuery(
                "SELECT s from AppBundle:Seat s where s.credit = :invoice ")
            ->setParameter(":invoice", $invoice)
            ->getResult();
    }

    public function findByPaymentTypeAndCleared($paymentType){
        return $this->getEntityManager()
            ->createQuery(
                "SELECT i from AppBundle:Invoice i where i.paymentType = :paymentType and i.cleared = 1 ")
            ->setParameter(":paymentType", $paymentType)
            ->getResult();
    }
   }
