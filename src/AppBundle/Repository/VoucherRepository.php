<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Voucher;
use AppBundle\Util\PaginationUtil;
use Doctrine\ORM\EntityRepository;

class VoucherRepository extends EntityRepository
{






    public function findAllPaged(){
        $query = $this->getEntityManager()
            ->createQuery(
                'SELECT v from AppBundle:Voucher v ORDER BY v.id desc')
        ;
        $paginationUtil = new PaginationUtil();
        $paginationUtil->setQuery($query);
        return $paginationUtil;
    }


}