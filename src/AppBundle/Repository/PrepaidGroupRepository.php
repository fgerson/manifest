<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Jumptype;
use AppBundle\Entity\PrepaidType;
use AppBundle\Entity\User;
use AppBundle\Util\PaginationUtil;
use Doctrine\ORM\EntityRepository;

class PrepaidGroupRepository extends EntityRepository
{

    public function findAllUnpaid(){

        return $this->getEntityManager()->createQuery(
            'SELECT p FROM AppBundle:PrepaidGroup p where p.invoice is null')
            ->getResult();
    }
    


    public function findUnpaidByPayer($payer){

        return $this->getEntityManager()->createQuery(
            'SELECT p FROM AppBundle:PrepaidGroup p where p.invoice is null and p.payer = :payer')
            ->setParameter(":payer", $payer)
            ->getResult();
    }

    public function findAllPaged(){

        $query =  $this->getEntityManager()->createQuery(
            'SELECT p FROM AppBundle:PrepaidGroup p ORDER BY p.id DESC')

        ;
        $pagination = new PaginationUtil();
        $pagination->setQuery($query);
        return $pagination;

    }

    public function findByUserPaged($user){

        $query =  $this->getEntityManager()->createQuery(
            'SELECT p FROM AppBundle:PrepaidGroup p WHERE p.payer = :user ORDER BY p.id DESC')
            ->setParameter(':user', $user)
        ;
        $pagination = new PaginationUtil();
        $pagination->setQuery($query);
        return $pagination;

    }

}