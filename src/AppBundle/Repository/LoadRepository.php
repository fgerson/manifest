<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Load;
use Doctrine\ORM\EntityRepository;

class LoadRepository extends EntityRepository
{
   public function getLoadsByDate(\DateTime $date){
   $date = $date->format("Y-m-d");
  
     return $this->getEntityManager()
            ->createQuery(
                'SELECT l FROM AppBundle:Load l WHERE SUBSTRING(l.created,1,10) = :date'
            )
            ->setParameter(':date', $date)
            
            ->getResult();
   }
   public function getLoadsOfDateAndNotStarted(\DateTime $dateTime){
       $date = $dateTime->format("Y-m-d");

       return $this->getEntityManager()
           ->createQuery(
               'SELECT l FROM AppBundle:Load l WHERE l.started is null AND SUBSTRING(l.created,1,10) = :date ORDER BY l.id ASC'
           )
           ->setParameter(':date', $date)

           ->getResult();
   }

    public function getLoadOfTheDayNumber(Load $load){
        $date = $load->getCreated()->format("Y-m-d");

        return $this->getEntityManager()
            ->createQuery(
                'SELECT count(l) as no FROM AppBundle:Load l WHERE SUBSTRING(l.created,1,10) = :date and l.id < :loadid '
            )
            ->setParameter(':date', $date)
            ->setParameter(':loadid', $load->getId())
            ->getResult()[0]['no'];
    }

    public function getLoadBefore(Load $load){
        $date = $load->getCreated()->format("Y-m-d");

        return $this->getEntityManager()
            ->createQuery(
                'SELECT l as no FROM AppBundle:Load l WHERE  SUBSTRING(l.started,1,10) = :date order by l.started DESC '
            )
            ->setParameter(':date', $date)

            ->getResult()[0]['no'];
    }
}