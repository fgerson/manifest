<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Jumptype;
use AppBundle\Entity\PrepaidType;
use AppBundle\Entity\User;
use AppBundle\Util\PaginationUtil;
use Doctrine\ORM\EntityRepository;

class PrepaidRepository extends EntityRepository
{

    




    public function findByUserAndJumpType(User $user, Jumptype $jumptype){

        return $this->getEntityManager()->createQuery(
            'SELECT p FROM AppBundle:Prepaid p inner join p.prepaid_type pt where p.user = :user AND pt.jumpType = :jumpType')
            ->setParameter(":user", $user)
            ->setParameter(":jumpType", $jumptype)
            ->getResult();
    }

}