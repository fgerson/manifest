<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SeatRepository extends EntityRepository
{
    public function findAllUncredits(){
        return $this->getEntityManager()->createQuery(
            'SELECT s FROM AppBundle:Seat s where s.credit is null and s.salaryType is not null')
            ->getResult();
    }

    public function findUncreditByCreditor($creditor)

    {  return $this->getEntityManager()
        ->createQuery(
            'SELECT s from AppBundle:Seat s WHERE s.credit is null and s.salaryType is not null and s.jumper = :creditor '

        )
        ->setParameter(":creditor", $creditor)
        ->getResult();
    }
   
}