<?php
namespace AppBundle\Twig;

use AppBundle\AppBundle;
use AppBundle\Entity\Voucher;
use AppBundle\Repository\VoucherRepository;
use AppBundle\Util\SeatCounter;

class TwigLoadExtension extends \Twig_Extension
{
    private $loadRepository;
    private $voucherRepository;
    public function __construct(\AppBundle\Repository\LoadRepository $loadRepository, VoucherRepository $voucherRepository)
    {
        $this->loadRepository = $loadRepository;
        $this->voucherRepository = $voucherRepository;
    }

    public function getFilters()
    {
       
        return array(
            new \Twig_SimpleFilter('seats', array($this, 'countUsedSeats')),
            new \Twig_SimpleFilter('getUsers', array($this, 'getUsers')),
            new \Twig_SimpleFilter('loadOfTheDay', array($this, 'loadOfTheDay')),
            new \Twig_SimpleFilter('jumpgroupDeleteable', array($this, 'isJumpGroupDeleteable')),
            new \Twig_SimpleFilter('minutesUntil', array($this, 'minutesUntil')),
            new \Twig_SimpleFilter('getJumpgroupForVoucher', array($this, 'getJumpgroupForVoucher')),
        );
    }

    public function countUsedSeats($load)
    {
        

        return SeatCounter::countSeats($load);
    }
    public function getUsers($jumpGroup){
        $users = array();

           foreach($jumpGroup->getSeats() as $seat){
            $users[] = $seat->getJumper();
      
        }
        return $users;
    }

    public function loadOfTheDay($load){
       return ($this->loadRepository->getLoadOfTheDayNumber($load) +1);

    }

    public function isJumpGroupDeleteable($jumpGroup){
        if($jumpGroup->getInvoice() != null){
           return false;
        }
        foreach($jumpGroup->getSeats() as $seat) {
            if($seat->getCredit() != null) return false;
        }
        return true;
    }

    public function minutesUntil(\DateTime $dateTime){
        $dateTime = $dateTime->setTimezone(new \DateTimeZone("Europe/Berlin"));
        $now = new \DateTime();
       
        $int = date_diff($now, $dateTime );

        return ($int->h * 60) + $int->i;
    }
    public function getJumpgroupForVoucher(Voucher $voucher){
       $jumpgroups = $this->voucherRepository->findJumpGroupForVoucher($voucher);
       if(count($jumpgroups) == 0){
           return null;
       }else{
           return $jumpgroups[0];
       }

    }

    
}